import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.harukaedu.keywords.Helper

WebUI.click(findTestObject('Studio/ClassesManagement/ImportDiscussion/BtnImportRed'))

WebUI.sendKeys(findTestObject('Studio/General/InpSearch'), unitDiscussionName)

WebUI.click(findTestObject('Studio/General/BtnSearch'))

Helper.defineWebElement("//a[.='$unitDiscussionName']/parent::span/parent::div/following-sibling::div/i", "xpath").click()

Helper.waitElementVisible("//div[@class='notif-container']//span[.='Import discussion to unit success']")