import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.harukaedu.keywords.Helper as Helper
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

WebUI.click(findTestObject('Studio/UnitsManagement/MenuUnitsManagement'))

WebUI.sendKeys(findTestObject('Studio/General/InpSearch'), unitName)

WebUI.click(findTestObject('Studio/General/BtnFilter'))

Helper.waitElementAndClick("//a[@title='$unitName']/following-sibling::a")

WebUI.delay(1)

def convertedActions = Helper.capitalizeFully(actions)

boolean isDelete = convertedActions == 'Delete' ? true : false

isDelete ? Helper.waitElementAndClick("//a[@title='$unitName']/following-sibling::a/following-sibling::ul//input[@value='$convertedActions']") : Helper.waitElementAndClick(
    "//a[@title='$unitName']/following-sibling::a/following-sibling::ul//a[contains(.,'$convertedActions')]")