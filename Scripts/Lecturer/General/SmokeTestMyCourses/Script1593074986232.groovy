import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.harukaedu.keywords.Helper as Helper
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

WebUI.waitForElementVisible(findTestObject('General/BtnBurgerMenu'), 10)

courseName = Helper.defineWebElement("//a[.='Kelas Ekonomi Mikro (EM01) A']", 'xpath')

Helper.waitElementAndClick(courseName)

Helper.waitElementVisible("//h2[contains(.,'Learning Activity')]")

WebUI.click(findTestObject('Lecturer/CourseInfo/MenuCourseInfo'))

Helper.waitElementVisible('//h2[.=\'infocourse info\']')

WebUI.click(findTestObject('Lecturer/Discussion/MenuDiscussion'))

Helper.waitElementVisible('//h2[contains(.,\'DISCUSSION\')]')

WebUI.click(findTestObject('Object Repository/Lecturer/Assignment/MenuAssignment'))

Helper.waitElementVisible('//h2[contains(.,\'Assignment\')]')

WebUI.click(findTestObject('Lecturer/OfflineAttendance/MenuOfflineAttendance'))

Helper.waitElementVisible("//h2[contains(.,'ATTENDANCE')]")

WebUI.click(findTestObject('Lecturer/Report/General/MenuReports'))

WebUI.click(findTestObject('Lecturer/Report/StudentAttendance/MenuStudentAttendance'))

Helper.waitElementVisible("//h2[contains(.,'Analytic')]")

WebUI.click(findTestObject("Lecturer/Report/LecturerAttendance/MenuLecturerAttendance"))

Helper.waitElementVisible("//h2[contains(.,'Lecturer Attendance')]")

WebUI.click(findTestObject("Lecturer/Report/Participant/MenuParticipant"))

Helper.waitElementVisible("//h2[contains(.,'Analytic')]")

Helper.waitElementVisible("//a[@class='active'][contains(text(),'Participation')]")

WebUI.click(findTestObject("Lecturer/Report/Performance/MenuPerformance"))

Helper.waitElementVisible("//h2[contains(.,'Analytic')]")

Helper.waitElementVisible("//a[@class='active'][contains(text(),'Performance')]")

WebUI.click(findTestObject("Lecturer/MinutesOfLectures/MenuMinutesOfLectures"))

Helper.waitElementVisible("//h2[contains(.,'Minutes of Lectures')]")

WebUI.click(findTestObject("Lecturer/Grade/MenuGrade"))

Helper.waitElementVisible('//h2[contains(.,\'Grade\')]')

WebUI.click(findTestObject("Lecturer/StudentAndGroups/MenuStudentAndGroups"))

Helper.waitElementVisible("//h2[contains(.,'Students & Groups')]")

WebUI.click(findTestObject("Lecturer/LiveSession/MenuLiveSession"))

Helper.waitElementVisible('//h2[contains(.,\'Live Session\')]')

WebUI.click(findTestObject("Object Repository/Lecturer/Message/MenuMessage"))

Helper.waitElementVisible('//h2[@class=\'message-title\']')