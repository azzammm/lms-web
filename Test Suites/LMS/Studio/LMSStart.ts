<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>LMSStart</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>f5d769f5-eada-4817-b42e-1dc090619609</testSuiteGuid>
   <testCaseLink>
      <guid>a44c96e1-6a7d-4002-8898-ee708a76c01d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Studio/ClassesManagement/Section/CreateSection</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d636dfd9-0959-4060-b24a-c996ca1f755e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Studio/ClassesManagement/Section/EditSection</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>799a7380-03b0-4ce1-b0fc-f5d411905647</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Studio/ClassesManagement/Unit/AddUnit</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8edb0a32-bfda-4414-abbc-65550f35e644</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Studio/ClassesManagement/Unit/AddContentsUnit</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>26f6db6b-a622-4a73-b043-3de12ea249bc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Studio/ClassesManagement/Unit/AddUnitDiscussion</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f6e45a58-3f9f-4475-88f3-bf4a36442651</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Studio/ClassesManagement/Unit/EditUnitDiscussion</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9a3a5c44-ba44-4792-8b85-e8b70ec700be</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Studio/ClassesManagement/Unit/DisableForumUnitDiscussion</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>afb60c81-f2ee-4c52-88cc-02a0b1095bcf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Studio/ClassesManagement/Unit/EnableForumUnitDiscussion</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>476e2dce-549a-4a1b-970d-29c0413367f9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Studio/ClassesManagement/Unit/EditUnit</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>142bb7fb-5b87-422c-a066-3d98b8200387</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Studio/ClassesManagement/Unit/EditUnitDiffDate</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>aae712dc-4044-4309-93de-93443eeb72a6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Studio/ClassesManagement/Unit/EditUnitDueDateBiggerThanStartDate</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>74b374d7-2732-4644-888a-4acf08643da1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Studio/ClassesManagement/Unit/EditUnitWithoutDate</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2a9cf58a-4c74-44c3-b79c-a12b66c571b7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Studio/ClassesManagement/Unit/EditUnitWithoutDueDate</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>464f36d1-5bb0-4c77-a18e-fcaa0f7663d4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Studio/ClassesManagement/Unit/EditUnitWithoutStartDate</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3e56bef3-6e82-4aa6-bf3b-d792c398d8c1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Studio/ClassesManagement/Unit/EditUnitWithStartDateBiggerThanToday</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e3d51db1-ef4a-4105-a598-e9ef9cbaae21</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Studio/ClassesManagement/PreviewLearningActivity/DueDateUnitDifferent</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d4a82d7a-eed1-4eba-81a1-edffc1c4d04d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Studio/ClassesManagement/PreviewLearningActivity/DateUnitEqualsDateSection</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cfdc40e7-118c-4ff6-8c6c-76ea3767d795</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Studio/ClassesManagement/PreviewLearningActivity/StartDateAndDueDateDifferent</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1c7b4346-1f9e-4887-a354-3009348df0c7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Studio/ClassesManagement/PreviewLearningActivity/StartDateUnitDifferent</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
