import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

WebUI.callTestCase(findTestCase('Studio/Essentials/CoursesManagement/General/EstlsCoursesAction'), [('courseName') : 'Demo'
        , ('actions') : 'Gradebook Template'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Studio/Essentials/CoursesManagement/GradeBook/EstlsCoursesManageComponentsGradeBook'), 
    [('gradeBookName') : 'Test offline section', ('sectionName') : 'Sesi 3', ('unitName') : 'Azzam change from Katalon'], FailureHandling.STOP_ON_FAILURE)