import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.harukaedu.keywords.Helper as Helper
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

WebUI.click(findTestObject('Studio/CoursesManagement/MenuCoursesManagement'))

WebUI.sendKeys(findTestObject('Studio/General/InpSearch'), courseName)

WebUI.click(findTestObject('Studio/General/BtnFilter'))

Helper.waitElementAndClick("//a[text()='$courseName']")