import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.harukaedu.keywords.Helper as Helper
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

if (title != '') {
	WebUI.clearText(findTestObject('Studio/AssetsManagement/Videos/InpTitle'))
	
    WebUI.sendKeys(findTestObject('Studio/AssetsManagement/Videos/InpTitle'), title)
}

if (description != '') {
	WebUI.clearText(findTestObject('Studio/AssetsManagement/Videos/InpDescription'))
	
    WebUI.sendKeys(findTestObject('Studio/AssetsManagement/Videos/InpDescription'), description)
}

if (isYoutube) {
    Helper.waitElementAndClick('//label[normalize-space()=\'Youtube\']')

    Helper.defineWebElement('//input[@id=\'youtube_video_id\']', 'xpath').sendKeys(videoId)
} else {
    Helper.waitElementAndClick('//label[normalize-space()=\'Kaltura\']')

    Helper.defineWebElement('//input[@id=\'kaltura_entry_id\']', 'xpath').sendKeys(videoId)
}

if (downloadUrl != '') {
	WebUI.clearText(findTestObject('Studio/AssetsManagement/Videos/InpDownloadUrl'))
	
    WebUI.sendKeys(findTestObject('Studio/AssetsManagement/Videos/InpDownloadUrl'), downloadUrl)
}

if (transcript != '') {
    Helper.uploadFile(findTestObject('Studio/AssetsManagement/Videos/UplTranscript'), transcript)
}

if (tags != '') {
	WebUI.clearText(findTestObject('Studio/AssetsManagement/Videos/InpTags'))
	
    WebUI.sendKeys(findTestObject('Studio/AssetsManagement/Videos/InpTags'), tags)
}

if (topic != '') {
	WebUI.clearText(findTestObject('Studio/AssetsManagement/Videos/InpTopic'))
	
    WebUI.sendKeys(findTestObject('Studio/AssetsManagement/Videos/InpTopic'), topic)
}

if (objective != '') {
	WebUI.clearText(findTestObject('Studio/AssetsManagement/Videos/InpObjective'))
	
    WebUI.sendKeys(findTestObject('Studio/AssetsManagement/Videos/InpObjective'), objective)
}

if (partner != '') {
    Helper.pickDropdown(findTestObject('Studio/AssetsManagement/Videos/SlctPartner'), partner)
}