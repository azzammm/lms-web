import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.harukaedu.keywords.Helper as Helper
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

def listContentName = listContents

def contentName

listContentName.size().times({ 
        contentName = (listContentName[it])

        WebUI.clearText(findTestObject('Studio/General/InpSearch'))

        WebUI.sendKeys(findTestObject('Studio/General/InpSearch'), contentName)

        WebUI.click(findTestObject('Studio/General/BtnSearch'))

        isDiscussion ? Helper.waitElementAndClick("//a[text()='$contentName']/parent::span/parent::div/preceding-sibling::p/label") : Helper.waitElementAndClick(
            "//span[contains(text(),'$contentName')]//parent::div//preceding-sibling::p[@class='asset-check']/label")

        isDiscussion ? Helper.waitElementVisible('//div[@class=\'toast\' and text()=\'Successfully add this discussion.\']') : Helper.waitElementVisible(
            '//div[@class=\'toast\' and text()=\'Successfully add this content.\']')
    })