import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.harukaedu.keywords.Helper as Helper
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

WebUI.click(findTestObject('Studio/ContentsManagement/MenuContentsManagement'))

listContentName.size().times({ 
        def contentName = listContentName[it]

        WebUI.sendKeys(findTestObject('Studio/General/InpSearch'), contentName)

        WebUI.click(findTestObject('Studio/General/BtnFilter'))

        Helper.waitElementAndClick("//a[@title='$contentName']/following-sibling::a")

        WebUI.delay(1)

        def convertedActions = Helper.capitalizeFully(actions)

        println(convertedActions)

        boolean isDelete = convertedActions == 'Delete' ? true : false

        println(isDelete)

        if (isDelete) {
            Helper.waitElementAndClick("//a[@title='$contentName']/following-sibling::a/following-sibling::ul//input[@value='$convertedActions']")

            WebUI.acceptAlert()

            Helper.waitElementVisible('//span[.=\'Content deleted\']//parent::div')
        } else {
            Helper.waitElementAndClick("//a[@title='$contentName']/following-sibling::a/following-sibling::ul//a[contains(.,'$convertedActions')]")
        }
    })