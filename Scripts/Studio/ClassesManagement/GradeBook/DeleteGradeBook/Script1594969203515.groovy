import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import com.harukaedu.keywords.Helper as Helper
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

def classNameConvert = Helper.capitalizeFully('Demo')

WebUI.callTestCase(findTestCase('Studio/Essentials/ClassesManagement/General/EstlsClassesAction'), [('className') : classNameConvert
        , ('actions') : 'Manage Gradebooks'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Studio/Essentials/ClassesManagement/GradeBook/EstlsGradeBookDelete'), [('gradebookName') : 'Grade Book From Katalon 2'], FailureHandling.STOP_ON_FAILURE)