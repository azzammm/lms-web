import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.harukaedu.keywords.Helper as Helper
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

if (title != '') {
	WebUI.clearText(findTestObject('Studio/AssetsManagement/Files/InpTitle'))
	
    WebUI.sendKeys(findTestObject('Studio/AssetsManagement/Files/InpTitle'), title)
}

if (description != '') {
	WebUI.clearText(findTestObject('Studio/AssetsManagement/Files/InpDescription'))
	
    WebUI.sendKeys(findTestObject('Studio/AssetsManagement/Files/InpDescription'), description)
}

if (file != '') {
    Helper.uploadFile(findTestObject('Studio/AssetsManagement/Files/UplFile'), file)
}

if (tags != '') {
	WebUI.clearText(findTestObject('Studio/AssetsManagement/Files/InpTags'))
	
    WebUI.sendKeys(findTestObject('Studio/AssetsManagement/Files/InpTags'), tags)
}

if (topic != '') {
	WebUI.clearText(findTestObject('Studio/AssetsManagement/Files/InpTopic'))
	
    WebUI.sendKeys(findTestObject('Studio/AssetsManagement/Files/InpTopic'), topic)
}

if (objective != '') {
	WebUI.clearText(findTestObject('Studio/AssetsManagement/Files/InpObjective'))
	
    WebUI.sendKeys(findTestObject('Studio/AssetsManagement/Files/InpObjective'), objective)
}

if (partner != '') {
    Helper.pickDropdown(findTestObject('Studio/AssetsManagement/Files/SlctPartner'), partner)
}