import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

WebUI.callTestCase(findTestCase('Studio/Essentials/CoursesManagement/General/EstlsSearchCourses'), [('courseName') : 'Demo'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Studio/Essentials/CoursesManagement/Unit/EstlsCoursesAddUnit'), [('sectionName') : 'SESI 3 - POLA KEGIATAN SUATU PEREKONOMIAN'
        , ('unitName') : 'Post Quiz Azzam'], FailureHandling.STOP_ON_FAILURE)