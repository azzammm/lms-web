import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.harukaedu.keywords.Common as Common
import com.harukaedu.keywords.Helper as Helper
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

def sectionName = 'Section Test From Katalon'

WebUI.callTestCase(findTestCase('Studio/Essentials/CoursesManagement/General/EstlsSearchCourses'), [('courseName') : 'Demo'], 
    FailureHandling.STOP_ON_FAILURE)

Helper.waitElementVisible("//h2[contains(.,'$sectionName')]")

def btnEditUnit = Helper.defineWebElement("//h2[contains(.,'$sectionName')]//ancestor::div/div/a[contains(.,'Edit Section')]", 
    'xpath')

Common.actions.moveToElement(btnEditUnit)

Common.actions.perform()

btnEditUnit.click()

WebUI.callTestCase(findTestCase('Test Cases/Studio/Essentials/CoursesManagement/Section/EstlsCoursesInputForm'), [('description') : 'From Katalon Edit'], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Studio/General/BtnSave'))