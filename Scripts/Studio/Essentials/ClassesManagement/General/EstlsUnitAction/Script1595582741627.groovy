import com.harukaedu.keywords.Helper as Helper
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

def dateMap = [('startDate') : '', ('dueDate') : '']

def menu

def regex = '\\b(0[0-9]|[12][0-9]|3[0-2])\\b ([A-Z][a-z])\\w+ (19|20)\\d{2} (00|[0-9]|1[0-9]|2[0-3]):([0-9]|[0-5][0-9])(.|\\n)'

Helper.waitElementVisible("//h2[.='$sectionName']")

textDate = Helper.defineWebElement("//h2[.='$sectionName']/parent::div/following-sibling::p[@class='date']", 'xpath').getText()

def regexResult = textDate =~ regex

dateMap.size().times({ 
        if (it == 0) {
            (dateMap['startDate']) = ((regexResult[it])[0])
        }
        
        if (it == 2) {
            (dateMap['dueDate']) = ((regexResult[it])[0])
        }
    })

def threeDots = Helper.defineWebElement("//h2[.='$sectionName']/parent::div/following-sibling::ul[@class='collection with-header']/li/span[.='$unitName']/following-sibling::a", 
    'xpath')

threeDots.click()

WebUI.delay(1)

if (action == 'Delete') {
    menu = Helper.defineWebElement("//h2[.='$sectionName']/parent::div/following-sibling::ul[@class='collection with-header']/li/span[.='$unitName']/following-sibling::ul/form/li/a/input", 
        'xpath')
} else {
    menu = Helper.defineWebElement("//h2[.='$sectionName']/parent::div/following-sibling::ul[@class='collection with-header']/li/span[.='$unitName']/following-sibling::ul/li/a[contains(.,'$action')]/i", 
        'xpath')
}

menu.click()

return dateMap