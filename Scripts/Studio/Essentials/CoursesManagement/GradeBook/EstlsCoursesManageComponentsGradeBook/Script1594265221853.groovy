import com.harukaedu.keywords.Helper as Helper

Helper.waitElementAndClick("//td[.='$gradeBookName']/following-sibling::td/a[.='Manage Components']")

//sectionName
Helper.defineWebElement("//h3[contains(.,'$sectionName')]//following-sibling::div//self::div//td[.='$unitName']/following-sibling::td/a[.='Components']", 
    'xpath').click()

//checklist content
Helper.defineWebElement('//div[@class=\'card quiz\']/p[@class=\'asset-check\']', 'xpath').click()

Helper.waitElementVisible('//div[.=\'Gradebooks component updated.\']')