import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.harukaedu.keywords.Helper as Helper

def sectionName = 'Section Test From Katalon'

WebUI.callTestCase(findTestCase('Studio/Essentials/CoursesManagement/General/EstlsSearchCourses'), [('courseName') : 'Demo'], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Studio/General/BtnAddRed'))

WebUI.callTestCase(findTestCase('Test Cases/Studio/Essentials/CoursesManagement/Section/EstlsCoursesInputForm'), [('sectionName') : sectionName
        , ('description') : 'From Katalon', ('dueDate') : 29, ('dueDateMonth') : 12, ('dueDateYear') : 2021, ('startDate') : 26
        , ('endDate') : 28, ('endDateMonth') : 12, ('endDateYear') : 2021, ('isAttendance') : false, ('sort') : '60', ('partnerName') : 'Demo'], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Studio/General/BtnSave'))

assert Helper.defineWebElement("//h2[.='$sectionName']", 'xpath').isDisplayed() == true