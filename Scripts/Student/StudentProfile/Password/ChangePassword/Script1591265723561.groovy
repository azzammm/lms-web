import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

def oldPassword = '123456789'
def newPassword = '12345678'

WebUI.click(findTestObject('General/BtnThreeDotMenu'))

WebUI.click(findTestObject('Student/Dashboard/StudentProfile/General/BtnEditProfile'))

WebUI.delay(1)

WebUI.click(findTestObject('Student/Dashboard/StudentProfile/Password/TabPassword'))

WebUI.delay(1)

WebUI.sendKeys(findTestObject('Student/Dashboard/StudentProfile/Password/InpCurrentPassword'), oldPassword)

WebUI.sendKeys(findTestObject('Student/Dashboard/StudentProfile/Password/InpNewPassword'), newPassword)

WebUI.sendKeys(findTestObject('Student/Dashboard/StudentProfile/Password/InpRetypeNewPassword'), newPassword)

WebUI.click(findTestObject('Student/Dashboard/StudentProfile/Password/BtnChangePassword'))

WebUI.delay(1)

println WebUI.getText(findTestObject('General/ToastContainer'))