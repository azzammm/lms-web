import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.harukaedu.keywords.Helper as Helper
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

WebUI.click(findTestObject('Object Repository/Studio/TopicsManagement/MenuTopicsManagement'))

WebUI.callTestCase(findTestCase('Studio/Essentials/TopicsManagement/EstlsUnitAction'), [('unitName') : 'Topic from Katalon'
        , ('actions') : 'Edit'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Studio/Essentials/TopicsManagement/EstlsInputForm'), [('key') : 'Topic from Katalon Edit', ('value') : 'Topic from Katalon Edit'
        , ('partner') : 'Demo'], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Studio/General/BtnSave'))

Helper.waitElementVisible('//span[.=\'Topic updated!\']//parent::div')