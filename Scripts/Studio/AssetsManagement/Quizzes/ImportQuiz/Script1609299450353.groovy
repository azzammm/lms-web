import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.harukaedu.keywords.Helper
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

WebUI.callTestCase(findTestCase('Studio/Essentials/AssetsManagement/EstlsDirectMenuAssets'), [('menu') : 'quizzes'], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Studio/General/BtnAddRed'))

WebUI.callTestCase(findTestCase('Studio/Essentials/AssetsManagement/Quizzes/EstlsInputFormQuizzes'), [('title') : 'Quiz from Katalon Import'
		, ('description') : 'Quiz from Katalon Import', ('tags') : 'tech, qa, test', ('totalTime') : '0', ('totalRandomQuestions') : '0'
		, ('partner') : 'Demo'], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Studio/AssetsManagement/Quizzes/BtnSaveAndAddQuestions'))

Helper.waitElementVisible('//span[.=\'Quiz created!\']//parent::div')

WebUI.click(findTestObject('Studio/AssetsManagement/Quizzes/BtnImportQuiz'))

Helper.uploadFile(findTestObject('Studio/AssetsManagement/Quizzes/UplFile'), "import_quiz.csv")

WebUI.click(findTestObject('Studio/AssetsManagement/Quizzes/BtnImportUploadFile'))

WebUI.click(findTestObject('Studio/AssetsManagement/Quizzes/BtnSaveImport'))

Helper.waitElementVisible('//span[.=\'Import Success!\']//parent::div')