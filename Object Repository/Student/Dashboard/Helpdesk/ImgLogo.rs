<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>ImgLogo</name>
   <tag></tag>
   <elementGuidId>2b96839f-4283-47d8-aae4-ab48ac66f358</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@class='center-screen']/img[@src='https://demo-lms.harukaeduapps.com/storage-files/ums%2F10-Demo%2Fpartners%2F10%2Flogo-63962.jpeg']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
