import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

def currentDate = new Date().format('dd')

def className = 'Kelas Aljabar (ALJBR) AAA'

WebUI.callTestCase(findTestCase('Studio/Essentials/ClassesManagement/General/EstlsSearchClass'), [('className') : className], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Studio/Essentials/ClassesManagement/Section/EstlsClassesInputFormSection'), [('action') : 'Save'
        , ('isEdit') : false, ('sectionName') : 'Section Create From Katalon', ('sectionDescription') : 'Test Essentials'
        , ('dueDate') : 29, ('dueDateMonth') : 12, ('dueDateYear') : 2025, ('startDate') : Integer.parseInt(currentDate)
        , ('endDate') : 30, ('endDateMonth') : 12, ('endDateYear') : 2025, ('partnerName') : 'Demo', ('sort') : ''], FailureHandling.STOP_ON_FAILURE)