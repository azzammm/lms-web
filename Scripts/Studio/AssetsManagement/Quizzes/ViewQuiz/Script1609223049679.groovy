import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.harukaedu.keywords.Helper as Helper
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

WebUI.callTestCase(findTestCase('Studio/Essentials/AssetsManagement/EstlsDirectMenuAssets'), [('menu') : 'quizzes'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Studio/Essentials/AssetsManagement/Quizzes/EstlsUnitAction'), [('unitName') : 'Quiz from Katalon'
        , ('actions') : 'View'], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Studio/AssetsManagement/Quizzes/BtnShowQuestion'))

Helper.waitElementVisible('//h2[contains(.,\'Question List\')]')

try {
    def totalQuestions = Helper.defineWebElements('//div[@class=\'question-section question question-block\']', 'xpath').size()

    totalQuestions == 0 ? Helper.waitElementVisible('//h4[normalize-space()=\'Belum ada pertanyaan dalam quiz ini.\']') : println(
        "Total question is : $totalQuestions")
}
catch (def ex) {
    println(ex)

    println('There\'s no question available')
}