import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.harukaedu.keywords.Common as Common
import com.harukaedu.keywords.Helper as Helper
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

WebUI.sendKeys(findTestObject('Studio/General/InpSearch'), unitName)

WebUI.click(findTestObject('Studio/General/BtnFilter'))

def card = Helper.defineWebElement("//a[.='$unitName']", 'xpath')

Helper.waitElementVisible(card)

def checklistCard = Helper.defineWebElement(card, '//parent::div/following-sibling::div//label', 'xpath')

checklistCard.click()

Helper.waitElementVisible("//div[@id='toast-container']")

WebUI.click(findTestObject('Object Repository/Studio/General/BtnBack'))
