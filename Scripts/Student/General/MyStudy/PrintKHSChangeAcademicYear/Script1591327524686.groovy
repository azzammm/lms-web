import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.harukaedu.keywords.Helper


WebUI.click(findTestObject('General/BtnBurgerMenu'))

WebUI.click(findTestObject('Student/Dashboard/MyStudy/MenuMyStudy'))

WebUI.waitForElementPresent(findTestObject('Student/Dashboard/MyStudy/SlctAcademicYear'), 10)

WebUI.click(findTestObject('Student/Dashboard/MyStudy/SlctAcademicYear'))

Helper.defineWebElement("//span[contains(text(),'EVEN - 2018-2019')]", "xpath").click()

WebUI.click(findTestObject('Student/Dashboard/MyStudy/BtnPrintKHS'))

WebUI.delay(5)