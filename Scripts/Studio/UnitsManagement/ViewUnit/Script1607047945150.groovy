import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase

import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

WebUI.callTestCase(findTestCase('Studio/Essentials/UnitsManagement/General/EstlsUnitAction'), [('unitName') : 'Test upload to 2 class'
	, ('actions') : 'View'], FailureHandling.STOP_ON_FAILURE)