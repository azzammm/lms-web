import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

WebUI.callTestCase(findTestCase('Studio/Essentials/AssetsManagement/EstlsDirectMenuAssets'), [('menu') : 'h5p'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Studio/Essentials/AssetsManagement/H5P/EstlsUnitAction'), [('unitName') : 'H5P from Katalon Edit'
        , ('actions') : 'View'], FailureHandling.STOP_ON_FAILURE)