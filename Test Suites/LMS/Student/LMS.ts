<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>LMS</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>5f558b7c-3ca7-4594-93fd-1ebad9c57e6f</testSuiteGuid>
   <testCaseLink>
      <guid>ab991fa8-659f-43ec-8b3d-b2d6072a5137</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Student/General/SmokeTestDashboard</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>183167b4-977c-4941-8d0d-e7930c76ce0b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Student/General/SmokeTestMyCourses</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
