import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.harukaedu.keywords.Helper as Helper
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

def listContentName = listContents

def contentName

listContentName.size().times({ 
        contentName = (listContentName[it])

        WebUI.clearText(findTestObject('Studio/General/InpSearch'))

        WebUI.sendKeys(findTestObject('Studio/General/InpSearch'), contentName)

        WebUI.click(findTestObject('Studio/General/BtnSearch'))

        Helper.defineWebElement("//a[contains(.,'$contentName')]/parent::span/parent::div/following-sibling::div//label", 
            'xpath').click()

        Helper.waitElementVisible('//div[.=\'Successfully add this content.\']')
    })