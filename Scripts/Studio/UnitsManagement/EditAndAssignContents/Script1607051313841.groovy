import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.harukaedu.keywords.Helper as Helper
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

WebUI.callTestCase(findTestCase('Studio/Essentials/UnitsManagement/General/EstlsUnitAction'), [('unitName') : 'Test upload to 2 class'
        , ('actions') : 'Edit'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Studio/Essentials/UnitsManagement/Unit/EstlsUnitInputForm'), [('description') : 'Edit from Katalon'], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Studio/UnitsManagement/Unit/BtnSaveContents'))

WebUI.callTestCase(findTestCase('Studio/Essentials/UnitsManagement/Unit/EstlsSearchUnits'), [('isDiscussion') : false, ('listContents') : [
            'SIM-LSPR-P-Issue and Crisis Introduction']], FailureHandling.STOP_ON_FAILURE)