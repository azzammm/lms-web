import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.harukaedu.keywords.Helper as Helper
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

WebUI.click(findTestObject('Studio/DiscussionsManagement/Discussion/MenuDiscussionsManagement'))

WebUI.click(findTestObject('Object Repository/Studio/General/BtnAddRed'))

WebUI.callTestCase(findTestCase('Studio/Essentials/DiscussionsManagement/EstlsInputForm'), [('title') : 'Forum from Katalon'
        , ('description') : 'Forum from Katalon', ('isForum') : true, ('tags') : 'tech, test, qa', ('minCharForReply') : '10'
        , ('partner') : 'Demo'], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Studio/DiscussionsManagement/Discussion/BtnSaveAndAddPost'))

Helper.waitElementVisible('//span[.=\'Discussion created!\']//parent::div')

WebUI.click(findTestObject('Studio/DiscussionsManagement/PostList/BtnAddPost'))

WebUI.callTestCase(findTestCase('Studio/Essentials/DiscussionsManagement/EstlsInputFormPost'), [('title') : 'Post Forum from Katalon'
		, ('body') : 'Post Forum from Katalon', ('maxReply') : '1000', ('availabilityRating') : 'Lecture', ('availabilityLike') : 'lecture and student'
		, ('availabilityExcellent') : 'student', ('partner') : 'Demo'], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Studio/General/BtnSave'))

Helper.waitElementVisible('//span[.=\'Post created!\']//parent::div')