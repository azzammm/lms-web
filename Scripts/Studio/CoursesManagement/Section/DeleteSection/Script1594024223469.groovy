import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import com.harukaedu.keywords.Common as Common
import com.harukaedu.keywords.Helper as Helper
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

def sectionName = 'Section Test From Katalon'

WebUI.callTestCase(findTestCase('Studio/Essentials/CoursesManagement/General/EstlsSearchCourses'), [('courseName') : 'Demo'], 
    FailureHandling.STOP_ON_FAILURE)

Helper.waitElementVisible("//h2[contains(.,'$sectionName')]")

def btnDelete = Helper.defineWebElement("//h2[contains(.,'$sectionName')]//ancestor::div/div/button", 'xpath')

Common.actions.moveToElement(btnDelete)

Common.actions.perform()

btnDelete.click()

WebUI.acceptAlert()

WebUI.delay(2)