import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.harukaedu.keywords.Helper as Helper
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

def listOfMap = []
def assetName
mapAssets.each{ k, values ->
    [values].flatten().eachWithIndex { value, index ->
        listOfMap[index] = listOfMap[index] ?: [:]
        listOfMap[index][k] = value
    }
}

for (int i = 0; i < listOfMap.size(); i++) {
    assetName = ((listOfMap['AssetName'])[i])

    if (assetName == null) {
        assetName = ((listOfMap['AssetName'])[(i - 1)])
    }
    
    println(assetName)

    def typeAsset = Helper.capitalizeFully((listOfMap['TypeAsset'])[i])

    WebUI.clearText(findTestObject('Studio/General/InpSearch'))

    WebUI.sendKeys(findTestObject('Studio/General/InpSearch'), assetName)

    WebUI.click(findTestObject('Studio/General/BtnSearch'))

    switch (typeAsset) {
        case 'Video':
            Helper.waitElementAndClick('//a[@href=\'#video\']')

            Helper.waitElementAndClick("//div[@id='video']//span[contains(text(),'$assetName')]/parent::div/preceding-sibling::p/label")

            break
        case 'Audio':
            Helper.waitElementAndClick('//a[@href=\'#audio\']')

            Helper.waitElementAndClick("//div[@id='audio']//span[contains(text(),'$assetName')]/parent::div/preceding-sibling::p/label")

            break
        case 'File':
            Helper.waitElementAndClick('//a[@href=\'#file\']')

            Helper.waitElementAndClick("//div[@id='file']//span[contains(text(),'$assetName')]/parent::div/preceding-sibling::p/label")

            break
        case 'Quiz':
            Helper.waitElementAndClick('//a[@href=\'#quiz\']')

            Helper.waitElementAndClick("//div[@id='quiz']//span[contains(text(),'$assetName')]/parent::div/preceding-sibling::p/label")

            break
        case 'H5p':
            Helper.waitElementAndClick('//a[@href=\'#h5p\']')

            Helper.waitElementAndClick("//div[@id='h5p']//span[contains(text(),'$assetName')]/parent::div/preceding-sibling::p/label")

            break
    }
    
    Helper.waitElementVisible('//div[@class=\'toast\' and text()=\'Successfully add this asset.\']')

    assetName = null
}