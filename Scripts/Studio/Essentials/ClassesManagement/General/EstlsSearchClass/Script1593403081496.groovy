import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.harukaedu.keywords.Helper as Helper
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

WebUI.sendKeys(findTestObject('Studio/General/InpSearch'), className)

WebUI.click(findTestObject('Studio/General/BtnFilter'))

Helper.waitElementAndClick("//small[text()='Publish']//preceding-sibling::a[text()='$className']")