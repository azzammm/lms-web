import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.harukaedu.keywords.Helper as Helper
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

WebUI.click(findTestObject('Studio/ContentsManagement/MenuContentsManagement'))

WebUI.callTestCase(findTestCase('Studio/Essentials/ContentsManagement/General/EstlsContentAction'), [('listContentName') : ['Content from Katalon']
        , ('actions') : 'Edit'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Studio/Essentials/ContentsManagement/Content/EstlsContentInputForm'), [('title') : 'Content from Katalon Edit'
        , ('description') : 'Test bikin dari Katalon Edit', ('tags') : 'tech, qa, test, edit', ('contentType') : 'Video'
        , ('attempts') : 'Once', ('partner') : 'Demo'], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Studio/ContentsManagement/Content/BtnSave'))

Helper.waitElementVisible('//span[.=\'Content updated!\']//parent::div')