import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.harukaedu.keywords.Helper as Helper
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

def discussionName

listDiscussions.size().times({ 
        discussionName = (listDiscussions[it])

        WebUI.clearText(findTestObject('Studio/General/InpSearch'))

        WebUI.sendKeys(findTestObject('Studio/General/InpSearch'), discussionName)

        WebUI.click(findTestObject('Studio/General/BtnSearch'))

        Helper.waitElementAndClick("//a[text()='$discussionName']/parent::span/parent::div/preceding-sibling::p/label")

        Helper.waitElementVisible('//div[@class=\'toast\' and text()=\'Successfully add this discussion.\']')
    })