import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.harukaedu.keywords.Helper as Helper
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

WebUI.sendKeys(findTestObject('Studio/General/InpSearch'), unitName)

WebUI.click(findTestObject('Studio/General/BtnFilter'))

Helper.waitElementAndClick("//span[contains(text(),'$unitName')]/parent::a/following-sibling::a")

WebUI.delay(1)

def convertedActions = Helper.capitalizeFully(actions)

switch (convertedActions) {
    case 'View':
        Helper.waitElementAndClick("//span[contains(text(),'$unitName')]/parent::a/following-sibling::a/following-sibling::ul//a[text()='View']")

        break
    case 'Edit':
        Helper.waitElementAndClick("//span[contains(text(),'$unitName')]/parent::a/following-sibling::a/following-sibling::ul//a[text()='Edit']")

        break
    case 'Delete':
        Helper.waitElementAndClick("//span[contains(text(),'$unitName')]/parent::a/following-sibling::a/following-sibling::ul//input[@value='Delete']")

        WebUI.acceptAlert()

        Helper.waitElementVisible('//span[.=\'Audio deleted\']//parent::div')

        break
    default:
        println('Please insert correct action!')}