import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.harukaedu.keywords.Helper as Helper

WebUI.callTestCase(findTestCase('Studio/Essentials/ClassesManagement/General/EstlsSearchClass'), [('className') : 'Kelas Aljabar (ALJBR) AAA'], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Studio/Essentials/ClassesManagement/General/EstlsUnitAction'), [('sectionName') : 'Section Create From Katalon'
        , ('unitName') : 'SIM-LSPR-S02-03-Issue Management', ('action') : 'Edit'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Studio/Essentials/ClassesManagement/Unit/EstlsClassesEditUnit'), [('action') : 'Save', ('startDate') : 0
        , ('dueDate') : 0], FailureHandling.STOP_ON_FAILURE)

Helper.waitElementVisible('//div[@class=\'card-panel teal lighten-2\']/span[.=\'Unit updated!\']')