import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.harukaedu.keywords.Helper as Helper
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

Helper.waitElementAndClick("//td[.='$gradeBookName']/following-sibling::td/a[.='Edit']")

WebUI.callTestCase(findTestCase('Studio/Essentials/CoursesManagement/GradeBook/EstlsCoursesInputFormGradeBook'), [('gradeBookName') : gradeBookNameChanged
        , ('percentage') : percentage, ('gradeBookType') : gradeBookType, ('examType') : examType, ('sectionType') : sectionType
        , ('isAverage') : isAverage, ('sort') : sort], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Studio/General/BtnSave'))