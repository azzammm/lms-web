import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

def listUnits = ['syahfitri,  mj20k,  ekonomi mikro,  uai, uts']

WebUI.callTestCase(findTestCase('Studio/Essentials/ClassesManagement/General/EstlsSearchClass'), [('className') : 'Kelas Manajemen Keamanan Informasi Lanjutan (AI51710015) A'], 
    FailureHandling.STOP_ON_FAILURE)

listUnits.size().times({ 
        WebUI.callTestCase(findTestCase('Studio/Essentials/ClassesManagement/General/EstlsSectionAction'), [('sectionName') : 'Section reproduce UTS error'
                , ('action') : 'Add Unit'], FailureHandling.STOP_ON_FAILURE)

        WebUI.callTestCase(findTestCase('Studio/Essentials/ClassesManagement/Unit/EstlsClassesAddUnit'), [('unitName') : listUnits[it]], 
            FailureHandling.STOP_ON_FAILURE)
    })