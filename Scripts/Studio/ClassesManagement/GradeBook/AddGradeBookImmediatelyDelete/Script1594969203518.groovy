import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.harukaedu.keywords.Helper as Helper
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

def classNameConvert = Helper.capitalizeFully('Demo')

def gradeBookName = 'Gradebook Exam from katalon'

WebUI.callTestCase(findTestCase('Studio/Essentials/ClassesManagement/General/EstlsClassesAction'), [('className') : classNameConvert
        , ('actions') : 'Manage Gradebooks'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Studio/Essentials/ClassesManagement/GradeBook/EstlsGradeBookInputForm'), [('action') : 'Save'
        , ('isEdit') : false, ('gradebookName') : '', ('gradebookNameInput') : gradeBookName, ('gradebookPercentage') : '10', ('gradebookType') : 'Video'
        , ('gradebookExamType') : '', ('gradebookSection') : '', ('gradebookIsAverage') : true, ('gradebookSort') : '1'], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Studio/Essentials/ClassesManagement/GradeBook/EstlsGradeBookDelete'), [('gradebookName') : gradeBookName], 
    FailureHandling.STOP_ON_FAILURE)

