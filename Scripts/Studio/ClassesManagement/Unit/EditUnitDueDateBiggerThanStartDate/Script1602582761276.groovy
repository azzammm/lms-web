import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import com.harukaedu.keywords.Helper as Helper
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

def currentDay = new Date().format('dd')

def currentDayConv = Integer.parseInt(currentDay)

println currentDayConv

WebUI.callTestCase(findTestCase('Studio/Essentials/ClassesManagement/General/EstlsSearchClass'), [('className') : 'Kelas Aljabar (ALJBR) AAA'], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Studio/Essentials/ClassesManagement/General/EstlsUnitAction'), [('sectionName') : 'Section Create From Katalon'
        , ('unitName') : 'SIM-LSPR-S02-03-Issue Management', ('action') : 'Edit'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Studio/Essentials/ClassesManagement/Unit/EstlsClassesEditUnit'), [('action') : 'Save', ('startDate') : currentDayConv+1
        , ('isClockStartMax') : false, ('dueDate') : currentDayConv, ('isClockDueMax') : true,], FailureHandling.STOP_ON_FAILURE)

Helper.waitElementVisible("//span[contains(text(),'The due time must be a date after start time.')]")