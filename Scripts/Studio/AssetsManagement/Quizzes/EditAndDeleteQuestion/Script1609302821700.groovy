import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

WebUI.callTestCase(findTestCase('Studio/Essentials/AssetsManagement/EstlsDirectMenuAssets'), [('menu') : 'quizzes'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Studio/Essentials/AssetsManagement/Quizzes/EstlsUnitAction'), [('unitName') : 'Quiz from Katalon Edit'
        , ('actions') : 'Edit'], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Studio/AssetsManagement/Quizzes/BtnQuestion'))

WebUI.callTestCase(findTestCase('Studio/Essentials/AssetsManagement/Quizzes/EstlsUnitQuestionAction'), [('questionNumber') : 1
        , ('action') : 'delete'], FailureHandling.STOP_ON_FAILURE)