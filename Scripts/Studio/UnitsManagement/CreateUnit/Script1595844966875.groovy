import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

WebUI.click(findTestObject('Studio/UnitsManagement/MenuUnitsManagement'))

WebUI.click(findTestObject('Studio/General/BtnAddRed'))

WebUI.callTestCase(findTestCase('Studio/Essentials/UnitsManagement/Unit/EstlsUnitInputForm'), [('title') : 'Unit create from Katalon'
        , ('description') : 'Create unit from katalon', ('dueDate') : 25, ('dueDateMonth') : 12, ('dueDateYear') : 2020, ('attempts') : 'Unlimited'
        , ('tags') : 'QA, Test, Katalon', ('topic') : '', ('objective') : '', ('partner') : 'Demo'], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Studio/UnitsManagement/Unit/BtnSave'))