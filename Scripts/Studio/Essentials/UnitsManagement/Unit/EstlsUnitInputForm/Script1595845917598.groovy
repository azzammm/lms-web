import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.harukaedu.keywords.Common as Common
import com.harukaedu.keywords.Helper as Helper
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

if (title != '') {
    WebUI.clearText(findTestObject('Object Repository/Studio/UnitsManagement/Unit/InpTitle'))

    WebUI.sendKeys(findTestObject('Object Repository/Studio/UnitsManagement/Unit/InpTitle'), title)
}

if (description != '') {
    WebUI.clearText(findTestObject('Object Repository/Studio/UnitsManagement/Unit/TxtDescription'))

    WebUI.sendKeys(findTestObject('Object Repository/Studio/UnitsManagement/Unit/TxtDescription'), description)
}

if (dueDate > 0) {
    Helper.pickDate(findTestObject('Object Repository/Studio/UnitsManagement/Unit/InpDueDate'), dueDate, dueDateMonth, dueDateYear)
}

if (attempts != '') {
    WebUI.click(findTestObject('Object Repository/Studio/UnitsManagement/Unit/SlctAttempts'))

    def object = Helper.defineWebElement("//span[contains(text(),'$attempts')]//parent::li", 'xpath')

    Common.actions.moveToElement(object)

    Common.actions.perform()

    object.click()
}

if (tags != '') {
    WebUI.clearText(findTestObject('Object Repository/Studio/UnitsManagement/Unit/InpTags'))

    WebUI.sendKeys(findTestObject('Object Repository/Studio/UnitsManagement/Unit/InpTags'), tags)
}

if (topic != '') {
    WebUI.clearText(findTestObject('Object Repository/Studio/UnitsManagement/Unit/InpTopic'))

    WebUI.sendKeys(findTestObject('Object Repository/Studio/UnitsManagement/Unit/InpTopic'), topic)
}

if (objective != '') {
    WebUI.clearText(findTestObject('Object Repository/Studio/UnitsManagement/Unit/InpObjective'))

    WebUI.sendKeys(findTestObject('Object Repository/Studio/UnitsManagement/Unit/InpObjective'), objective)
}

if (partner != '') {
    Helper.pickDropdown(findTestObject('Object Repository/Studio/UnitsManagement/Unit/SlctPartner'), partner)
}