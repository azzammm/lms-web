import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import com.harukaedu.keywords.Helper as Helper
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

WebUI.callTestCase(findTestCase('Studio/Essentials/UnitsManagement/General/EstlsUnitAction'), [('unitName') : 'Unit create from Katalon'
        , ('actions') : 'Delete'], FailureHandling.STOP_ON_FAILURE)

WebUI.acceptAlert()

Helper.waitElementVisible('//span[.=\'Unit deleted!\']//parent::div')