import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.harukaedu.keywords.Helper as Helper
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

def materialName = 'PKO-S03-Pre Quiz'

Helper.waitElementAndClick('//div[@class=\'panel\']//div[1]//h3[1]')

Helper.pickDropdown(findTestObject('Student/MyCourses/General/SlctClasses'), 'Kelas Aljabar (ALJBR) A')

WebUI.click(findTestObject('Student/MyCourses/LearningActivity/MenuLearningActivity'))

Helper.pickDropdown(findTestObject('Student/MyCourses/LearningActivity/SlctSection'), '3')

def learningMaterial = Helper.defineWebElement("//a[contains(text(),'$materialName')]/parent::div", 'xpath')

assert learningMaterial.getAttribute('class').contains('material-list panel') == true

learningMaterial.click()

Helper.waitElementAndClick('//span[contains(text(),\'PKO-S03-Prequiz/Postquiz\')]//following-sibling::a')

def totalQuiz = Helper.defineWebElements('//div[@class=\'question-list\']/div', 'xpath').size()

for (int i = 0; i < totalQuiz; i++) {
    def button = Helper.defineWebElement('//a[contains(text(),\' next\')]', 'xpath')

    if (button.isDisplayed()) {
        button.click()
    } else {
        Helper.defineWebElement('//button[@id=\'submit_question\']', 'xpath').click()
    }
    
    WebUI.delay(1)
}

assert Helper.defineWebElement('//h2[contains(text(),\'Your score is\')]', 'xpath').isDisplayed() == true

Helper.defineWebElement('//a[contains(text(),\'Done\')]', 'xpath').click()