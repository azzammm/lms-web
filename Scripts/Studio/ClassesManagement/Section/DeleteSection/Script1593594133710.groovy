import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

def className = 'Kelas Aljabar (ALJBR) AAA'

WebUI.callTestCase(findTestCase('Studio/Essentials/ClassesManagement/General/EstlsSearchClass'), [('className') : className], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Studio/Essentials/ClassesManagement/General/EstlsSectionAction'), [('sectionName') : 'Section Create From Katalon'
        , ('action') : 'Delete'], FailureHandling.STOP_ON_FAILURE)