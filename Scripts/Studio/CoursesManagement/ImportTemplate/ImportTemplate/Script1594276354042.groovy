import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

WebUI.callTestCase(findTestCase('Studio/Essentials/CoursesManagement/General/EstlsCoursesAction'), [('courseName') : 'Demo', ('actions') : 'Import Template'], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Studio/Essentials/CoursesManagement/ImportTemplate/EstlsImportTemplate'), [('fileName') : 'course_template_import.csv'], 
    FailureHandling.STOP_ON_FAILURE)