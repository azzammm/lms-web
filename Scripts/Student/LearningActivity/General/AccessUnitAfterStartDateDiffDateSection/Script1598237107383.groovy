import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.harukaedu.keywords.Helper as Helper
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

def materialName = 'SIM-LSPR-S02-03-Issue Management'

Helper.waitElementAndClick('//div[@class=\'panel\']//div[1]//h3[1]')

Helper.pickDropdown(findTestObject('Student/MyCourses/General/SlctClasses'), 'Kelas Aljabar (ALJBR) AAA')

WebUI.click(findTestObject('Student/MyCourses/LearningActivity/MenuLearningActivity'))

Helper.pickDropdown(findTestObject('Student/MyCourses/LearningActivity/SlctSection'), '3')

def learningMaterial = Helper.defineWebElement("//a[contains(text(),'$materialName')]/parent::div", 'xpath')

assert learningMaterial.getAttribute('class').contains('material-list panel') == true

assert Helper.defineWebElement("//a[contains(text(),'$materialName')]/parent::div/div[@class='material-startdate']", 'xpath').isDisplayed() == 
true

learningMaterial.click()

