import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.harukaedu.keywords.Common as Common
import com.harukaedu.keywords.Helper as Helper
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

if (action == 'Save') {
    if (isEdit == false) {
        WebUI.click(findTestObject('Studio/General/BtnAddRed'))
    }
    
    if (sectionName != '') {
        WebUI.clearText(findTestObject('Studio/ClassesManagement/Section/InpName'))

        WebUI.sendKeys(findTestObject('Studio/ClassesManagement/Section/InpName'), sectionName)
    }
    
    if (sectionDescription != '') {
        WebUI.clearText(findTestObject('Studio/ClassesManagement/Section/InpDescription'))

        WebUI.sendKeys(findTestObject('Studio/ClassesManagement/Section/InpDescription'), sectionDescription)
    }
    
    if (startDate != 0) {
        Helper.pickDate(findTestObject('Studio/ClassesManagement/Section/InpStartDate'), startDate, startDateMonth, startDateYear)

        WebUI.delay(1)
    }
    
    if (endDate != 0) {
        Helper.pickDate(findTestObject('Studio/ClassesManagement/Section/InpEndDate'), endDate, endDateMonth, endDateYear)

        WebUI.delay(1)
    }
    
    if (dueDate != 0) {
        Helper.pickDate(findTestObject('Studio/ClassesManagement/Section/InpDueDate'), dueDate, dueDateMonth, dueDateYear)

        WebUI.delay(1)
    }
    
    if (partnerName != '') {
        WebUI.click(findTestObject('Studio/ClassesManagement/Section/SlctPartner'))

        WebUI.delay(1)

        def element = Helper.defineWebElement("//span[contains(text(),'$partnerName')]", 'xpath')

        Common.actions.moveToElement(element)

        Common.actions.perform()

        element.click()
    }
    
    if (sort != '') {
        WebUI.clearText(findTestObject('Studio/ClassesManagement/Section/InpSort'))

        WebUI.sendKeys(findTestObject('Studio/ClassesManagement/Section/InpSort'), sort)
    }
    
    WebUI.delay(1)

    WebUI.click(findTestObject('Studio/General/BtnSave'))

    Helper.waitElementVisible('//div[@class=\'notif-container\']')
}

if (action == 'Cancel') {
    WebUI.click(findTestObject('Studio/General/BtnCancel'))
}