import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

def String[] attributes = ["50px","#000000", "87%"]

WebUI.click(findTestObject('General/BtnBurgerMenu'))

WebUI.delay(1)

WebUI.click(findTestObject('Student/Dashboard/Helpdesk/MenuHelpdesk'))

WebUI.delay(1)

WebUI.verifyElementPresent(findTestObject('Student/Dashboard/Helpdesk/ImgLogo'), 5)

WebUI.verifyElementPresent(findTestObject('Student/Dashboard/Helpdesk/IconLiveHelp'), 5)

WebUI.verifyElementPresent(findTestObject('Student/Dashboard/Helpdesk/TxtAnnounce'), 5)

assert WebUI.getText(findTestObject('Student/Dashboard/Helpdesk/TxtAnnounce')) == 'Helpdesk service is not availbale' 

def style = WebUI.getAttribute(findTestObject('Student/Dashboard/Helpdesk/IconLiveHelp'), 'style')

attributes.each {
	KeywordUtil.markPassed("Validating attributes...")
	style.contains("[$it]")
	KeywordUtil.markPassed("Validation successfull with attribute : $it")
}
	
