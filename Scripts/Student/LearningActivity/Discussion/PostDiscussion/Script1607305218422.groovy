import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.harukaedu.keywords.Helper as Helper
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

def materialName = 'SIM-LSPR-S02-Diskusi Minggu 2'

def message = 'Testing automation nich bruh 1'

Helper.defineWebElement('//a[contains(text(),\'Kelas Aljabar (ALJBR) L\')]', 'xpath').click()

Helper.pickDropdown(findTestObject('Student/MyCourses/LearningActivity/SlctSection'), '14')

def learningMaterial = Helper.defineWebElement("//a[contains(text(),'$materialName')]/parent::div", 'xpath')

learningMaterial.click()

WebUI.click(findTestObject('Object Repository/Student/MyCourses/LearningActivity/Discussion/BtnPostAnswer'))

WebUI.delay(2)

def messageToSend = Helper.defineWebElement('//textarea[@id=\'body\']', 'xpath')

messageToSend.sendKeys("$message")

def buttonSave = Helper.defineWebElement(messageToSend, '//parent::div/following-sibling::div[@class=\'actions\']/button', 
    'xpath')

Helper.actions.moveToElement(buttonSave)

Helper.actions.perform()

buttonSave.click()

Helper.waitElementVisible("//a-scene[contains(.,'$message')]")