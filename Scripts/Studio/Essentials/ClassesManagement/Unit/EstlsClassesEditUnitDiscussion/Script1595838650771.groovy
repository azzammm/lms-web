import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.harukaedu.keywords.Helper as Helper
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

if (action == 'Save') {
    if (title != '') {
        WebUI.clearText(findTestObject('Studio/ClassesManagement/Unit/Discussion/InpTitle'))

        WebUI.sendKeys(findTestObject('Studio/ClassesManagement/Unit/Discussion/InpTitle'), title)
    }
    
    if (isGrouped == true) {
        WebUI.scrollToElement(findTestObject('Studio/ClassesManagement/Unit/Discussion/IsGrouped'), 1)

        WebUI.click(findTestObject('Studio/ClassesManagement/Unit/Discussion/IsGrouped'))
    }
    
    if (isLecturerOnly == true) {
        WebUI.scrollToElement(findTestObject('Studio/ClassesManagement/Unit/Discussion/IsLecturerOnly'), 1)

        WebUI.click(findTestObject('Studio/ClassesManagement/Unit/Discussion/IsLecturerOnly'))
    }
    
    if (isCloseDiscussion == true) {
        WebUI.scrollToElement(findTestObject('Studio/ClassesManagement/Unit/Discussion/IsCloseDiscussion'), 1)

        WebUI.click(findTestObject('Studio/ClassesManagement/Unit/Discussion/IsCloseDiscussion'))
    }
    
    if (minimumCharacters != '') {
        WebUI.clearText(findTestObject('Studio/ClassesManagement/Unit/Discussion/InpMinimumCharacter'))

        WebUI.sendKeys(findTestObject('Studio/ClassesManagement/Unit/Discussion/InpMinimumCharacter'), minimumCharacters)
    }
    
    if (tags != '') {
        WebUI.clearText(findTestObject('Studio/ClassesManagement/Unit/Discussion/InpTags'))

        WebUI.sendKeys(findTestObject('Studio/ClassesManagement/Unit/Discussion/InpTags'), tags)
    }
    
    if (partner != '') {
        Helper.pickDropdown(findTestObject('Studio/ClassesManagement/Unit/Discussion/SlctPartner'), partner)
    }
    
    WebUI.click(findTestObject('Studio/General/BtnSave'))

    Helper.waitElementVisible('//div[@class=\'card-panel teal lighten-2\']//span[.=\'Section unit discussion updated\']')
}

if (action == 'Cancel') {
    WebUI.click(findTestObject('Studio/General/BtnCancel'))
}