import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

WebUI.click(findTestObject('Studio/ContentsManagement/MenuContentsManagement'))

WebUI.callTestCase(findTestCase('Studio/Essentials/ContentsManagement/General/EstlsContentAction'), [('listContentName') : [
            'Content from Katalon Edit', 'Content from Katalon Assign Asset Edit', 'Content from Katalon Assign Discussion Edit'], ('actions') : 'delete'], FailureHandling.STOP_ON_FAILURE)