import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.click(findTestObject('General/BtnBurgerMenu'))

WebUI.click(findTestObject('Student/Dashboard/StudyPlan/MenuStudyPlan'))

WebUI.click(findTestObject('General/BtnBurgerMenu'))

WebUI.click(findTestObject('Student/Dashboard/MyStudy/MenuMyStudy'))

WebUI.click(findTestObject('General/BtnBurgerMenu'))

WebUI.click(findTestObject('Student/Dashboard/MyTranscript/MenuMyTranscript'))

WebUI.click(findTestObject('General/BtnBurgerMenu'))

WebUI.click(findTestObject('Object Repository/Student/Dashboard/News/MenuNews'))

WebUI.click(findTestObject('General/BtnBurgerMenu'))

WebUI.click(findTestObject('Object Repository/Student/Dashboard/Calendar/MenuCalendar'))

WebUI.click(findTestObject('General/BtnBurgerMenu'))

WebUI.click(findTestObject('Object Repository/Student/Dashboard/Thesis/MenuThesis'))

WebUI.click(findTestObject('General/BtnBurgerMenu'))

WebUI.click(findTestObject('Object Repository/Student/Dashboard/Elibrary/MenuElibrary'))

WebUI.click(findTestObject('General/BtnBurgerMenu'))

WebUI.click(findTestObject('Object Repository/Student/Dashboard/MyCourse/MenuMyCourse'))

WebUI.click(findTestObject('General/BtnBurgerMenu'))

WebUI.click(findTestObject('Object Repository/Student/Dashboard/Helpdesk/MenuHelpdesk'))

WebUI.click(findTestObject('General/BtnBurgerMenu'))

WebUI.click(findTestObject('Object Repository/Student/Dashboard/TermsAndConditions/MenuTermsAndConditions'))