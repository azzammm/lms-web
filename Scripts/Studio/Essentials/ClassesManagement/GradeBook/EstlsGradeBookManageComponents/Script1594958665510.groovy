import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import com.harukaedu.keywords.Helper as Helper
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

WebUI.sendKeys(findTestObject('Studio/General/InpSearch'), className)

WebUI.click(findTestObject('Studio/General/BtnFilter'))

Helper.waitElementVisible("//a[.='$className']//following-sibling::a")

def threeDotsMenu = Helper.defineWebElement("//a[.='$className']//following-sibling::a", 'xpath')

threeDotsMenu.click()

Helper.defineWebElement(threeDotsMenu, '//following-sibling::ul/li/a[contains(.,\'Manage Gradebooks\')]', 'xpath').click()

if (typeCommand == 'Manage Component') {
    Helper.defineWebElement("//td[.='$gradebookName']/following-sibling::td/a[.='Manage Component']", 'xpath').click()

    Helper.defineWebElement("//td[.='$unitName']/following-sibling::td/a", 'xpath').click()

    Helper.defineWebElement("//span[contains(.,'$contentName')]//ancestor::div/p", '').click()

    Helper.waitElementVisible('//div[@id=\'toast-container\']')
}