import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.harukaedu.keywords.Helper as Helper
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

if (title != '') {
	WebUI.clearText(findTestObject('Studio/AssetsManagement/Quizzes/InpTitle'))
	
    WebUI.sendKeys(findTestObject('Studio/AssetsManagement/Quizzes/InpTitle'), title)
}

if (description != '') {
	WebUI.clearText(findTestObject('Studio/AssetsManagement/Quizzes/InpDescription'))
	
    WebUI.sendKeys(findTestObject('Studio/AssetsManagement/Quizzes/InpDescription'), description)
}

if (tags != '') {
	WebUI.clearText(findTestObject('Studio/AssetsManagement/Quizzes/InpTags'))
	
    WebUI.sendKeys(findTestObject('Studio/AssetsManagement/Quizzes/InpTags'), tags)
}

if (totalTime != '') {
	WebUI.clearText(findTestObject('Studio/AssetsManagement/Quizzes/InpTotalTime'))
	
    WebUI.sendKeys(findTestObject('Studio/AssetsManagement/Quizzes/InpTotalTime'), totalTime)
}

if (totalRandomQuestions != '') {
	WebUI.clearText(findTestObject('Studio/AssetsManagement/Quizzes/InpRandomQuestions'))
	
    WebUI.sendKeys(findTestObject('Studio/AssetsManagement/Quizzes/InpRandomQuestions'), totalRandomQuestions)
}

if (topic != '') {
	WebUI.clearText(findTestObject('Studio/AssetsManagement/Quizzes/InpTopic'))
	
    WebUI.sendKeys(findTestObject('Studio/AssetsManagement/Quizzes/InpTopic'), topic)
}

if (objective != '') {
	WebUI.clearText(findTestObject('Studio/AssetsManagement/Quizzes/InpObjective'))
	
    WebUI.sendKeys(findTestObject('Studio/AssetsManagement/Quizzes/InpObjective'), objective)
}

if (partner != '') {
    Helper.pickDropdown(findTestObject('Studio/AssetsManagement/Quizzes/SlctPartner'), partner)
}	