import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.harukaedu.keywords.Helper as Helper
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

if (isAllSection == false) {
    def className = Helper.defineWebElement('//div[@class=\'card-content\']//a', 'xpath').getText()

    WebUI.click(findTestObject('Studio/CoursesManagement/ExportTemplate/RbtnSelectedSections'))

    WebUI.click(findTestObject('Studio/CoursesManagement/ExportTemplate/SlctSection'))

    listSections.size().times({ 
            println(it)

            def section = listSections[(it - 1)]

            Helper.defineWebElement("//span[.='$section']", 'xpath').click()
        })

    return className
}