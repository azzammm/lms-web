import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.harukaedu.keywords.Helper as Helper
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

WebUI.click(findTestObject('Studio/CoursesManagement/MenuCoursesManagement'))

WebUI.click(findTestObject('Studio/CoursesManagement/AllPartners'))

WebUI.delay(1)

Helper.defineWebElement('//li/span[text()=\'Demo\']', 'xpath').click()

WebUI.delay(1)

WebUI.click(findTestObject('Studio/CoursesManagement/AllFaculties'))

WebUI.delay(1)

Helper.defineWebElement('//li/span[text()=\'Demo LMS\']', 'xpath').click()

WebUI.delay(1)

WebUI.click(findTestObject('Studio/CoursesManagement/AllDegreeProgrammes'))

WebUI.delay(1)

Helper.defineWebElement('//li/span[text()=\'Demo LMS 001\']', 'xpath').click()

WebUI.delay(1)

WebUI.click(findTestObject('Studio/CoursesManagement/AllAcademicYears'))

WebUI.delay(1)

Helper.defineWebElement('//li/span[text()=\'ODD 2019-2020\']', 'xpath').click()

WebUI.delay(1)