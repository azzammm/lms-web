import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.harukaedu.keywords.Helper as Helper
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

if (title != '') {
	WebUI.clearText(findTestObject('Studio/AssetsManagement/Audios/InpTitle'))
	
    WebUI.sendKeys(findTestObject('Studio/AssetsManagement/Audios/InpTitle'), title)
}

if (description != '') {
	WebUI.clearText(findTestObject('Studio/AssetsManagement/Audios/InpDescription'))
	
    WebUI.sendKeys(findTestObject('Studio/AssetsManagement/Audios/InpDescription'), description)
}

if (soundCloudTrackId != '') {
	WebUI.clearText(findTestObject('Studio/AssetsManagement/Audios/InpSoundcloudTrackId'))
	
    WebUI.sendKeys(findTestObject('Studio/AssetsManagement/Audios/InpSoundcloudTrackId'), soundCloudTrackId)
}

if (downloadUrl != '') {
	WebUI.clearText(findTestObject('Studio/AssetsManagement/Audios/InpDownloadUrl'))
	
    WebUI.sendKeys(findTestObject('Studio/AssetsManagement/Audios/InpDownloadUrl'), downloadUrl)
}

if (transcript != '') {
    Helper.uploadFile(findTestObject('Studio/AssetsManagement/Audios/UplTranscript'), transcript)
}

if (tags != '') {
	WebUI.clearText(findTestObject('Studio/AssetsManagement/Audios/InpTags'))
	
    WebUI.sendKeys(findTestObject('Studio/AssetsManagement/Audios/InpTags'), tags)
}

if (topic != '') {
	WebUI.clearText(findTestObject('Studio/AssetsManagement/Audios/InpTopic'))
	
    WebUI.sendKeys(findTestObject('Studio/AssetsManagement/Audios/InpTopic'), topic)
}

if (objective != '') {
	WebUI.clearText(findTestObject('Studio/AssetsManagement/Audios/InpObjective'))
	
    WebUI.sendKeys(findTestObject('Studio/AssetsManagement/Audios/InpObjective'), objective)
}

if (partner != '') {
    Helper.pickDropdown(findTestObject('Studio/AssetsManagement/Audios/SlctPartner'), partner)
}