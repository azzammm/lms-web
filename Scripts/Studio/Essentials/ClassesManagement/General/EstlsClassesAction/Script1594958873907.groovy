import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.harukaedu.keywords.Helper as Helper
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

WebUI.sendKeys(findTestObject('Studio/General/InpSearch'), className)

WebUI.click(findTestObject('Studio/General/BtnFilter'))

Helper.waitElementAndClick("//a[.='$className']/following-sibling::a")

WebUI.delay(1)

def convertedActions = Helper.capitalizeFully(actions)

Helper.waitElementAndClick("//a[.='$className']/following-sibling::a/following-sibling::ul//a[contains(.,'$convertedActions')]")