import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import com.harukaedu.keywords.Helper as Helper
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

WebUI.callTestCase(findTestCase('Studio/Essentials/ClassesManagement/General/EstlsSearchClass'), [('className') : 'Kelas Aljabar (ALJBR) AAA'], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Studio/Essentials/ClassesManagement/General/EstlsUnitAction'), [('sectionName') : 'Section Create From Katalon'
        , ('unitName') : 'SIM-LSPR-S02-03-Issue Management', ('action') : 'Delete'], FailureHandling.STOP_ON_FAILURE)

WebUI.delay(1)

for (int i = 0; i < 2; i++) {
    WebUI.acceptAlert()

    WebUI.delay(1)
}

Helper.waitElementVisible('//div[@class=\'card-panel teal lighten-2\']/span[.=\'Unit successfully removed!\']')