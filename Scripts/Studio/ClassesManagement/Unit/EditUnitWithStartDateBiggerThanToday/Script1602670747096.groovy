import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import com.harukaedu.keywords.Helper as Helper
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

def currentDay = new Date().format('dd')

def currentDayConv = Integer.parseInt(currentDay)

println(currentDayConv)

WebUI.callTestCase(findTestCase('Studio/Essentials/ClassesManagement/General/EstlsSearchClass'), [('className') : 'Kelas Aljabar (ALJBR) AAA'], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Studio/Essentials/ClassesManagement/General/EstlsUnitAction'), [('sectionName') : 'Section Create From Katalon'
        , ('unitName') : 'PKO-S03-02-Pemrosesan', ('action') : 'Edit'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Studio/Essentials/ClassesManagement/Unit/EstlsClassesEditUnit'), [('action') : 'Save', ('startDate') : currentDayConv + 
        1, ('isClockStartMax') : false, ('dueDate') : currentDayConv + 2, ('isClockDueMax') : true], FailureHandling.STOP_ON_FAILURE)

Helper.waitElementVisible('//div[@class=\'card-panel teal lighten-2\']/span[.=\'Unit updated!\']')