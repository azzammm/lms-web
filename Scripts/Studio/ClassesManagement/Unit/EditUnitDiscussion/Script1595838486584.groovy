import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

WebUI.callTestCase(findTestCase('Studio/Essentials/ClassesManagement/General/EstlsSearchClass'), [('className') : 'Kelas Aljabar (ALJBR) AAA'], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Studio/Essentials/ClassesManagement/General/EstlsUnitAction'), [('sectionName') : 'Section Create From Katalon'
        , ('unitName') : 'SIM-LSPR-S02-Diskusi Minggu 2', ('action') : 'Unit Discussions'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Studio/Essentials/ClassesManagement/Unit/EstlsClassesDiscussionAction'), [('discussionName') : 'Discussion QA Engineer'
        , ('action') : 'Edit'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Studio/Essentials/ClassesManagement/Unit/EstlsClassesEditUnitDiscussion'), [('action') : 'Save'
        , ('title') : 'Discussion QA Engineer', ('isGrouped') : false, ('isLecturerOnly') : false, ('isCloseDiscussion') : false
        , ('minimumCharacters') : '', ('tags') : '', ('partner') : ''], FailureHandling.STOP_ON_FAILURE)