import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.harukaedu.keywords.Helper as Helper
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

if (action == 'Save') {
    if (title != '') {
        WebUI.clearText(findTestObject('Studio/ClassesManagement/Unit/InpUnit'))

        WebUI.sendKeys(findTestObject('Studio/ClassesManagement/Unit/InpUnit'), title)
    }
    
    if (description != '') {
        WebUI.clearText(findTestObject('Studio/ClassesManagement/Unit/InpDescription'))

        WebUI.sendKeys(findTestObject('Studio/ClassesManagement/Unit/InpDescription'), description)
    }
    
    if (startDate > 0) {
        Helper.pickDateTime(findTestObject('Studio/ClassesManagement/Unit/InpStartDate'), startDate, startMonth, startYear, isClockStartMax)
    }
    
    if (startDate == 0) {
        WebUI.clearText(findTestObject('Studio/ClassesManagement/Unit/InpStartDate'))
    }
    
    if (dueDate > 0) {
        Helper.pickDateTime(findTestObject('Studio/ClassesManagement/Unit/InpDueDate'), dueDate, dueMonth, dueYear, isClockDueMax)
    }
    
    if (dueDate == 0) {
        WebUI.clearText(findTestObject('Studio/ClassesManagement/Unit/InpDueDate'))
    }
    
    if (sort != '') {
        WebUI.clearText(findTestObject('Studio/ClassesManagement/Unit/InpSort'))

        WebUI.sendKeys(findTestObject('Studio/ClassesManagement/Unit/InpSort'), sort)
    }
    
    if (attempts != '') {
        Helper.pickDropdown(findTestObject('Studio/ClassesManagement/Unit/SlctAttempts'), attempts)
    }
    
    if (tags != '') {
        WebUI.clearText(findTestObject('Studio/ClassesManagement/Unit/InpTags'))

        WebUI.sendKeys(findTestObject('Studio/ClassesManagement/Unit/InpTags'), tags)
    }
    
    if (topic != '') {
        WebUI.sendKeys(findTestObject('Studio/ClassesManagement/Unit/InpTopic'), topic)
    }
    
    if (objective != '') {
        WebUI.clearText(findTestObject('Studio/ClassesManagement/Unit/InpObjective'))

        WebUI.sendKeys(findTestObject('Studio/ClassesManagement/Unit/InpObjective'), objective)
    }
    
    if (partner != '') {
        Helper.pickDropdown(findTestObject('Studio/ClassesManagement/Unit/SlctPartner'), partner)
    }
    
    Helper.waitElementAndClick('//button[@value=\'save\' or contains(.,\'Save\')][2]')
}