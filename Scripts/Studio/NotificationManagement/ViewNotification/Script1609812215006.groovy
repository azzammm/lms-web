import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

WebUI.click(findTestObject('Object Repository/Studio/NotificationManagement/General/MenuNotificationManagement'))

WebUI.callTestCase(findTestCase('Studio/Essentials/NotificationManagement/EstlsUnitAction'), [('unitName') : 'Notification from Katalon'
        , ('actions') : 'View'], FailureHandling.STOP_ON_FAILURE)