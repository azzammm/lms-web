import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.harukaedu.keywords.Common as Common
import com.harukaedu.keywords.Helper as Helper
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

for (int i = 0; i < mapQuestions.size(); i++) {
    Helper.waitElementVisible('//iframe[@title=\'Rich Text Editor, question\']')
	
	Helper.defineWebElement('//input[@checked=\'checked\']', 'xpath').click()

    def randomNumber = Math.abs(new Random().nextInt() % (mapQuestions[i].answer.size() - 0)) + 0

    def iFrameQuestions = Helper.defineWebElement('//iframe[@title=\'Rich Text Editor, question\']', 'xpath')

    Common.driver.switchTo().frame(iFrameQuestions)

    def questionField = Helper.defineWebElement('//body', 'xpath')

    questionField.clear()

    questionField.sendKeys(mapQuestions[i].question)

    Common.driver.switchTo().parentFrame()

    def totalAnswer = mapQuestions[i].answer.size()

    def totalCurrentAnswer = Helper.defineWebElements('//input[contains(@name,\'is_answer\')]', 'xpath').size()

    def loop = totalAnswer - totalCurrentAnswer

    if (totalAnswer > totalCurrentAnswer) {
        for (int j = 0; j < loop; j++) {
            Helper.defineWebElement('//span[contains(.,\'Add Answer\')]', 'xpath').click()
        }
    } else {
        for (int j = 0; j < Math.abs(loop); j++) {
			WebUI.scrollToPosition(0, 0)
			
            obj = Helper.defineWebElements('//label[contains(@class,\'checkbox option-choice question-option\')]/span[@class=\'delete-option\']', 
                'xpath')

            total = obj.size()

            def randomNumbers = Math.abs(new Random().nextInt() % (total - 0)) + 0

            (obj[randomNumbers]).click()
        }
    }
    
    def iFrameAnswers = Helper.defineWebElements('//label[contains(@class,\'checkbox option-choice question-option\')]/span[@class=\'delete-option\']/following-sibling::div[contains(@id,\'cke\')]//iframe', 
        'xpath')

    for (int k = 0; k < totalAnswer; k++) {
        Common.driver.switchTo().frame(iFrameAnswers[k])

        def answerField = Helper.defineWebElement('//body', 'xpath')

        answerField.clear()

        answerField.sendKeys(mapQuestions[i].answer[k])

        Common.driver.switchTo().parentFrame()
    }

    WebUI.scrollToPosition(0, 0)

    Helper.defineWebElements("//input[contains(@name,'is_answer')]", 'xpath')[randomNumber].click()
}