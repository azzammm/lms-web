import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.harukaedu.keywords.Common as Common
import com.harukaedu.keywords.Helper as Helper
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

if (title != '') {
    WebUI.clearText(findTestObject('Object Repository/Studio/DiscussionsManagement/Post/InpTitle'))

    WebUI.sendKeys(findTestObject('Object Repository/Studio/DiscussionsManagement/Post/InpTitle'), title)
}

if (body != '') {
    iFrame = Helper.defineWebElement('//iframe[@title=\'Rich Text Editor, body\']', 'xpath')

    Common.driver.switchTo().frame(iFrame)

	Helper.waitElementVisible("//body")
	
    bodyPost = Helper.defineWebElement('//body', 'xpath')

    bodyPost.clear()

    bodyPost.sendKeys(body)

    Common.driver.switchTo().parentFrame()
}

if (maxReply != '') {
    WebUI.clearText(findTestObject('Object Repository/Studio/DiscussionsManagement/Post/InpMaxReply'))

    WebUI.sendKeys(findTestObject('Object Repository/Studio/DiscussionsManagement/Post/InpMaxReply'), maxReply)
}

switch (Helper.capitalizeFully(availabilityRating)) {
    case 'Lecture':
        Helper.waitElementAndClick('//label[@for=\'lecturer_rating_enabled\']')

        break
    case 'Student':
        Helper.waitElementAndClick('//label[@for=\'student_rating_enabled\']')

        break
    case 'Lecture And Student':
        Helper.waitElementAndClick('//label[@for=\'lecturer_rating_enabled\']')

        Helper.waitElementAndClick('//label[@for=\'student_rating_enabled\']')

        break
    default:
        println('Please input the correct rating')}

switch (Helper.capitalizeFully(availabilityLike)) {
    case 'Lecture':
        Helper.waitElementAndClick('//label[@for=\'lecturer_like_enabled\']')

        break
    case 'Student':
        Helper.waitElementAndClick('//label[@for=\'student_like_enabled\']')

        break
    case 'Lecture And Student':
        Helper.waitElementAndClick('//label[@for=\'lecturer_like_enabled\']')

        Helper.waitElementAndClick('//label[@for=\'student_like_enabled\']')

        break
    default:
        println('Please input the correct like')}

switch (Helper.capitalizeFully(availabilityExcellent)) {
    case 'Lecture':
        Helper.waitElementAndClick('//label[@for=\'lecturer_excellent_enabled\']')

        break
    case 'Student':
        Helper.waitElementAndClick('//label[@for=\'student_excellent_enabled\']')

        break
    case 'Lecture And Student':
        Helper.waitElementAndClick('//label[@for=\'lecturer_excellent_enabled\']')

        Helper.waitElementAndClick('//label[@for=\'student_excellent_enabled\']')

        break
    default:
        println('Please input the correct excellent')}

if (partner != '') {
    Helper.pickDropdown(findTestObject('Object Repository/Studio/DiscussionsManagement/Post/SlctPartner'), partner)
}