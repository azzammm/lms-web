import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

def listSections = ['Aljabar Azzam QA', 'Aljabar Katalon QA']

WebUI.callTestCase(findTestCase('Studio/Essentials/CoursesManagement/General/EstlsCoursesAction'), [('courseName') : 'Aljabar Linear'
        , ('actions') : 'Export Template'], FailureHandling.STOP_ON_FAILURE)

def className = WebUI.callTestCase(findTestCase('Studio/Essentials/CoursesManagement/ExportTemplate/EstlsCoursesExportTemplate'), 
    [('isAllSection') : false, ('listSections') : listSections], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Studio/CoursesManagement/ExportTemplate/BtnExportTemplate'))

WebUI.click(findTestObject('Studio/ClassesManagement/MenuClassesManagement'))

WebUI.callTestCase(findTestCase('Studio/Essentials/ClassesManagement/General/EstlsSearchClass'), [('className') : "$className"], 
    FailureHandling.STOP_ON_FAILURE)