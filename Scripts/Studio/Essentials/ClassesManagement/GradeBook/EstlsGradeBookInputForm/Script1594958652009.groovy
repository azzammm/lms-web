import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.harukaedu.keywords.Helper as Helper
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

if (action == 'Save') {
    if (isEdit == true) {
        Helper.defineWebElement("//td[.='$gradebookName']/following-sibling::td/a[.='Edit']", 'xpath').click()
    } else {
        WebUI.click(findTestObject('Studio/General/BtnAddRed'))
    }
    
    if (gradebookNameInput != '') {
        WebUI.clearText(findTestObject('Studio/ClassesManagement/GradeBooks/InpName'))

        WebUI.sendKeys(findTestObject('Studio/ClassesManagement/GradeBooks/InpName'), gradebookNameInput)
    }
    
    if (gradebookPercentage != '') {
        WebUI.clearText(findTestObject('Studio/ClassesManagement/GradeBooks/InpPercentage'))

        WebUI.sendKeys(findTestObject('Studio/ClassesManagement/GradeBooks/InpPercentage'), gradebookPercentage)
    }
    
    if (gradebookType != '') {
        Helper.pickDropdown(findTestObject('Studio/ClassesManagement/GradeBooks/SlctGradebookType'), gradebookType)
    }
    
    if (gradebookExamType != '') {
        Helper.pickDropdown(findTestObject('Studio/ClassesManagement/GradeBooks/SlctExamType'), gradebookExamType)

        Helper.pickDropdown(findTestObject('Studio/ClassesManagement/GradeBooks/SlctGradebookSection'), gradebookSection)
    }
    
    WebUI.delay(1)

    if (gradebookIsAverage == true) {
        WebUI.click(findTestObject('Studio/ClassesManagement/GradeBooks/RbtnAverageYes'))
    }
    
    if (gradebookIsAverage == false) {
        WebUI.click(findTestObject('Studio/ClassesManagement/GradeBooks/RbtnAverageNo'))
    }
    
    if (gradebookSort != '') {
        WebUI.clearText(findTestObject('Studio/ClassesManagement/GradeBooks/InpSort'))

        WebUI.sendKeys(findTestObject('Studio/ClassesManagement/GradeBooks/InpSort'), gradebookSort)
    }
    
    WebUI.click(findTestObject('Studio/General/BtnSave'))

    Helper.waitElementVisible('//div[@class=\'notif-container\']')
}

if (action == 'Cancel') {
    WebUI.click(findTestObject('Object Repository/Studio/General/BtnCancel'))
}