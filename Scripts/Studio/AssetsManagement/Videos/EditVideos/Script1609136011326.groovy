import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.harukaedu.keywords.Helper as Helper
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

WebUI.callTestCase(findTestCase('Studio/Essentials/AssetsManagement/EstlsDirectMenuAssets'), [('menu') : 'videos'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Studio/Essentials/AssetsManagement/Videos/EstlsUnitAction'), [('unitName') : 'Video from Katalon'
        , ('actions') : 'Edit'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Studio/Essentials/AssetsManagement/Videos/EstlsInputFormVideo'), [('title') : 'Video from Katalon Edit'
        , ('description') : 'Video from Katalon Edit', ('isYoutube') : true, ('videoId') : '8Y4WCdJRfV4', ('transcript') : 'transcript_videos.txt'
        , ('tags') : 'tech, test, qa', ('partner') : 'Demo'], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Studio/AssetsManagement/Videos/BtnSaveEdit'))

Helper.waitElementVisible('//span[.=\'Video updated!\']//parent::div')