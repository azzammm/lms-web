import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import com.harukaedu.keywords.Common as Common
import com.harukaedu.keywords.Helper as Helper
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

def className = 'Kelas Perilaku Organisasi  (MAN209) B'

WebUI.callTestCase(findTestCase('Studio/Essentials/ClassesManagement/General/EstlsSearchClass'), [('className') : className], 
    FailureHandling.STOP_ON_FAILURE)

def totalData = Helper.defineWebElements('//a[contains(text(),\'Edit Section\')]', 'xpath').size()

for (int i = 8; i < totalData; i++) {
    def btnAction = Helper.defineWebElements('//a[contains(text(),\'Edit Section\')]', 'xpath')

    Helper.waitElementVisible(btnAction[i])

    Common.actions.moveToElement(btnAction[i]).perform()

    (btnAction[i]).click()

    WebUI.callTestCase(findTestCase('Studio/Essentials/ClassesManagement/Section/EstlsClassesInputFormSection'), [('action') : 'Save'
        , ('isEdit') : true], FailureHandling.STOP_ON_FAILURE)
}