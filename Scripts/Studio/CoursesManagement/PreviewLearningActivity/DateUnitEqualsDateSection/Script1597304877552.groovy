import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.harukaedu.keywords.Helper as Helper
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

def dateMapUnit = [('startDate') : '', ('dueDate') : '']

def dateMapSection

def courseName = 'Aljabar'

def sectionName = 'Section Katalon'

def unitName = 'SIM-LSPR-S02-03-Issue Management'

WebUI.callTestCase(findTestCase('Studio/Essentials/CoursesManagement/General/EstlsSearchCourses'), [('courseName') : courseName], 
    FailureHandling.STOP_ON_FAILURE)

dateMapSection = WebUI.callTestCase(findTestCase('Studio/Essentials/CoursesManagement/General/EstlsCoursesSectionAction'), 
    [('sectionName') : sectionName, ('actions') : 'add unit'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Studio/Essentials/CoursesManagement/Unit/EstlsCoursesAddUnit'), [('unitName') : unitName], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Studio/Essentials/CoursesManagement/General/EstlsCoursesUnitAction'), [('sectionName') : sectionName
        , ('unitName') : unitName, ('actions') : 'Edit'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Studio/Essentials/CoursesManagement/Unit/EstlsCoursesEditUnit'), [('startDate') : 13, ('dueDate') : 29], 
    FailureHandling.STOP_ON_FAILURE)

Helper.waitElementVisible("//h2[.='$sectionName']")

textDate = Helper.defineWebElement("//h2[.='$sectionName']/parent::div/following-sibling::ul/li/span[.='$unitName']/following-sibling::p[@class='date']", 
    'xpath').getText()

def regexResult = textDate =~ '\\b(0[0-9]|[12][0-9]|3[0-2])\\b ([A-Z][a-z])\\w+ (19|20)\\d{2} ([0-9]|[0-5][0-9]):([0-9]|[0-5][0-9])(.|\\n)'

dateMapUnit.size().times({ 
        println((regexResult[it])[0])

        if (it == 0) {
            (dateMapUnit['startDate']) = ((regexResult[it])[0])
        }
        
        if (it == 1) {
            (dateMapUnit['dueDate']) = ((regexResult[it])[0])
        }
    })

WebUI.click(findTestObject('Studio/CoursesManagement/MenuCoursesManagement'))

WebUI.callTestCase(findTestCase('Studio/Essentials/CoursesManagement/General/EstlsCoursesAction'), [('actions') : 'Preview Learning Activity'
        , ('courseName') : courseName], FailureHandling.STOP_ON_FAILURE)

Helper.pickDropdown(findTestObject('Studio/ClassesManagement/PreviewLearningActivity/SlctSection'), '11')

if (((dateMapSection['startDate']) == (dateMapUnit['startDate'])) && ((dateMapSection['endDate']) == (dateMapUnit['endDate']))) {
    assert Helper.defineWebElement("//a[contains(.,'$unitName')]/following-sibling::div[@class='material-startdate']", 'xpath').isDisplayed() == 
    false

    KeywordUtil.markPassed('Validation successful!')
} else {
    KeywordUtil.markFailed('Validation failed! start date section != start date unit and end date section != end date unit')
}