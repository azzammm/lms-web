import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.harukaedu.keywords.Helper as Helper
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

WebUI.click(findTestObject('Studio/DiscussionsManagement/Discussion/MenuDiscussionsManagement'))

WebUI.callTestCase(findTestCase('Studio/Essentials/DiscussionsManagement/EstlsUnitAction'), [('unitName') : 'Discussion from Katalon Edit'
        , ('actions') : 'Edit'], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Studio/DiscussionsManagement/Discussion/BtnShowPost'))

WebUI.click(findTestObject('Studio/DiscussionsManagement/PostList/BtnAddPost'))

WebUI.callTestCase(findTestCase('Studio/Essentials/DiscussionsManagement/EstlsInputFormPost'), [('title') : 'Add Post Existing from Katalon'
        , ('body') : 'Add Post Existing from Katalon', ('maxReply') : '1000', ('availabilityRating') : 'Lecture', ('availabilityLike') : 'lecture and student'
        , ('availabilityExcellent') : 'student', ('partner') : 'Demo'], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Studio/General/BtnSave'))

Helper.waitElementVisible('//span[.=\'Post created!\']//parent::div')