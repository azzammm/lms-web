import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.harukaedu.keywords.Helper as Helper
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

Helper.defineWebElement("//td[.='$gradebookName']/following-sibling::td/a[.='Delete']", 'xpath').click()

Helper.driver.switchTo().alert().accept();

Helper.waitElementVisible("//div[@class='card-panel teal lighten-2']//span[.='Gradebook deleted.']")