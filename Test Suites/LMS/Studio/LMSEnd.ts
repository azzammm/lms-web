<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>LMSEnd</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>3000</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>c350433b-cecb-4af2-9716-979b98da7f8a</testSuiteGuid>
   <testCaseLink>
      <guid>6973443e-848a-46ae-a506-bf0ea091714a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Studio/ClassesManagement/Unit/DeleteUnitDiscussion</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>00ac108b-6ae4-4b59-a709-b49d95e83ea3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Studio/ClassesManagement/Unit/DeleteUnit</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>76165867-b5a8-4882-bc7b-705911b8101a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Studio/ClassesManagement/Section/DeleteSection</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>06327c2f-ca81-48bc-ab16-a14d762c967f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Studio/ClassesManagement/Section/DeleteAllSection</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
