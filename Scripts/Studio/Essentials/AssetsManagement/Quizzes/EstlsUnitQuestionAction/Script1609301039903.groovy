import com.harukaedu.keywords.Helper as Helper
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

numQuest = (questionNumber - 1)

actionConv = Helper.capitalizeFully(action)

Helper.waitElementAndClick("//a[@data-activates='question_$numQuest']")

WebUI.delay(1)

switch (actionConv) {
    case 'Edit':
        Helper.waitElementAndClick("//a[@data-activates='question_$numQuest']/following-sibling::ul//a[contains(.,'Edit')]")

        break
    case 'Delete':
        Helper.waitElementAndClick("//a[@data-activates='question_$numQuest']/following-sibling::ul//button[contains(.,'Delete')]")

        WebUI.acceptAlert()

        Helper.waitElementVisible('//span[.=\'Question deleted\']//parent::div')

        break
    case 'History':
        Helper.waitElementAndClick("//a[@data-activates='question_$numQuest']/following-sibling::ul//a[contains(.,'history')]")

        break
}