import com.harukaedu.keywords.Common as Common
import com.harukaedu.keywords.Helper as Helper
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

def btnAction

def convertedActions = Helper.capitalizeFully(actions)

Helper.waitElementVisible("//h2[contains(.,'$sectionName')]")

threeDots = Helper.defineWebElement("//h2[contains(.,'$sectionName')]/parent::div/following-sibling::ul//span[.='$unitName']/following-sibling::a", 
    'xpath')

Common.actions.moveToElement(threeDots).perform()

threeDots.click()

btnAction = Helper.defineWebElement("//h2[contains(.,'$sectionName')]/parent::div/following-sibling::ul//span[.='$unitName']/following-sibling::ul/li/a[contains(.,'$actions')]/i", 
    'xpath')

WebUI.delay(1)

btnAction.click()