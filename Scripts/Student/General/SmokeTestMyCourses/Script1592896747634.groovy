import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.harukaedu.keywords.Helper as Helper
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

WebUI.waitForElementVisible(findTestObject('General/BtnBurgerMenu'), 10)

courseName = Helper.defineWebElement('//a[contains(.,\'Kelas Aljabar (ALJBR) D\')]', 'xpath')

Helper.waitElementAndClick(courseName)

WebUI.click(findTestObject('Student/MyCourses/CourseInfo/MenuCourseInfo'))

Helper.waitElementVisible('//h2[.=\'infocourse info\']')

WebUI.click(findTestObject('Student/MyCourses/Discussion/MenuDiscussion'))

Helper.waitElementVisible('//h2[contains(.,\'DISCUSSION\')]')

WebUI.click(findTestObject('Student/MyCourses/Attendance/MenuAttendance'))

Helper.waitElementVisible('//h2[contains(.,\'Attendance\')]')

WebUI.click(findTestObject('Student/MyCourses/Analytics/MenuAnalytics'))

Helper.waitElementVisible('//h2[contains(.,\'Analytic\')]')

WebUI.click(findTestObject('Student/MyCourses/Grade/MenuGrade'))

Helper.waitElementVisible('//h2[contains(.,\'Grade\')]')

WebUI.click(findTestObject('Student/MyCourses/ClassmateAndGroup/MenuClassmateAndGroup'))

Helper.waitElementVisible('//h2[contains(.,\'Classmate & Group\')]')

WebUI.click(findTestObject('Student/MyCourses/PeerReview/MenuPeerReview'))

Helper.waitElementVisible('//h2[contains(.,\'Peer Review\')]')

WebUI.click(findTestObject('Student/MyCourses/Message/MenuMessage'))

Helper.waitElementVisible('//h2[@class=\'message-title\']')

WebUI.click(findTestObject('Student/MyCourses/LiveSession/MenuLiveSession'))

Helper.waitElementVisible('//h2[contains(.,\'Live Session\')]')