import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

WebUI.click(findTestObject('Studio/ContentsManagement/MenuContentsManagement'))

WebUI.click(findTestObject('Studio/General/BtnAddRed'))

WebUI.callTestCase(findTestCase('Studio/Essentials/ContentsManagement/Content/EstlsContentInputForm'), [('title') : 'Content from Katalon Assign Discussion'
        , ('description') : 'Test bikin dari Katalon', ('tags') : 'tech, qa, test', ('contentType') : 'Final Exam', ('attempts') : 'Once'
        , ('partner') : 'Demo'], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Studio/ContentsManagement/Content/BtnSaveAssignDiscussion'))

WebUI.callTestCase(findTestCase('Studio/Essentials/ContentsManagement/Content/EstlsSearchDiscussions'), [('listDiscussions') : ['MSDM-S02-WD', 'AKB-S02-WD']], 
    FailureHandling.STOP_ON_FAILURE)