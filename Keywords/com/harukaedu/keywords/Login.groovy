package com.harukaedu.keywords

import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.testng.annotations.Test

import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

@Test
public void test() {
	WebUI.openBrowser(GlobalVariable.url)

	WebUI.delay(1)

	Common.setUp()

	WebUI.sendKeys(findTestObject('Login/InpEmail'), GlobalVariable.email)

	WebUI.sendKeys(findTestObject('Login/InpPassword'), GlobalVariable.password)

	WebUI.click(findTestObject('Login/BtnLogin'))

	WebUI.waitForElementVisible(findTestObject('Studio/General/BtnFilter'), 10)
}
