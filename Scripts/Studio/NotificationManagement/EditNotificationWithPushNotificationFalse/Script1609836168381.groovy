import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.harukaedu.keywords.Helper as Helper
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

WebUI.click(findTestObject('Object Repository/Studio/NotificationManagement/General/MenuNotificationManagement'))

WebUI.callTestCase(findTestCase('Studio/Essentials/NotificationManagement/EstlsUnitAction'), [('unitName') : 'Notification from Katalon'
        , ('actions') : 'Edit'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Studio/Essentials/NotificationManagement/EstlsInputForm'), [('recipients') : ['Lecturer']
        , ('title') : 'Notification from Katalon Edit', ('body') : 'Notificaiton from Katalon Edit', ('url') : 'www.google.com'
        , ('isPushNotification') : false], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Studio/General/BtnSave'))

Helper.waitElementVisible('//span[.=\'Notification updated!\']//parent::div')