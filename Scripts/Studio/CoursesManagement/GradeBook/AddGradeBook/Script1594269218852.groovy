import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import com.harukaedu.keywords.Helper as Helper
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

WebUI.callTestCase(findTestCase('Studio/Essentials/CoursesManagement/General/EstlsCoursesAction'), [('courseName') : 'Demo'
        , ('actions') : 'Gradebook Template'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Studio/Essentials/CoursesManagement/GradeBook/EstlsCoursesAddGradeBook'), [('gradeBookName') : 'Test From Katalon'
        , ('percentage') : '20', ('gradeBookType') : 'Video', ('examType') : '', ('sectionType') : '', ('isAverage') : false
        , ('sort') : '3'], FailureHandling.STOP_ON_FAILURE)

Helper.defineWebElement('//div[@class=\'notif-container\']/span[.=\'Gradebook template updated.\']', 'xpath').isDisplayed()