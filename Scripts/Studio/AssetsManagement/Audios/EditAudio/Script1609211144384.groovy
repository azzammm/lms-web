import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.harukaedu.keywords.Helper

WebUI.callTestCase(findTestCase('Studio/Essentials/AssetsManagement/EstlsDirectMenuAssets'), [('menu') : 'audios'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Studio/Essentials/AssetsManagement/Audios/EstlsUnitAction'), [('unitName') : 'Audio from Katalon'
        , ('actions') : 'Edit'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Studio/Essentials/AssetsManagement/Audios/EstlsInputFormAudio'), [('title') : 'Audio from Katalon Edit'
        , ('description') : 'Audio from Katalon Edit', ('soundCloudTrackId') : '830177242', ('transcript') : 'transcript_audio.txt'
        , ('tags') : 'tech, test, qa', ('partner') : 'Demo'])

WebUI.click(findTestObject('Object Repository/Studio/General/BtnSave'))

Helper.waitElementVisible('//span[.=\'Audio updated!\']//parent::div')