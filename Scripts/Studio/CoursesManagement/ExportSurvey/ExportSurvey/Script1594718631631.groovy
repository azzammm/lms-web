import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.harukaedu.keywords.Helper as Helper
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

WebUI.callTestCase(findTestCase('Studio/Essentials/CoursesManagement/General/EstlsCoursesAction'), [('courseName') : 'Aljabar'
        , ('actions') : 'Export Survey'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Studio/Essentials/CoursesManagement/ExportSurvey/EstlsExportSurvey'), [('classNameList') : [
            'Kelas Aljabar (ALJBR) C']], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Studio/CoursesManagement/ExportSurvey/BtnExportSurvey'))

Helper.driver.switchTo().alert().accept()

Helper.defineWebElement('//div[@class=\'notif-container\']//span[.=\'Survey successfully exported to class\']', 'xpath').isDisplayed()