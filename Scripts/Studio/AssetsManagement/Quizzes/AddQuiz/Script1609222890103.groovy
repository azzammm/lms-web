import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.harukaedu.keywords.Helper as Helper
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

listMaps = [[('question') : 'udah makan belum?', ('answer') : ['Sudah', 'Belum','Udah 2 kali','Belum deh kaya nya']], [('question') : 'udah minum belum?', ('answer') : [
            'Sudah lagii', 'Belum lagii']]]

WebUI.callTestCase(findTestCase('Studio/Essentials/AssetsManagement/EstlsDirectMenuAssets'), [('menu') : 'quizzes'], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Studio/General/BtnAddRed'))

WebUI.callTestCase(findTestCase('Studio/Essentials/AssetsManagement/Quizzes/EstlsInputFormQuizzes'), [('title') : 'Quiz from Katalon'
        , ('description') : 'Quiz from Katalon', ('tags') : 'tech, qa, test', ('totalTime') : '0', ('totalRandomQuestions') : '0'
        , ('partner') : 'Demo'], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Studio/AssetsManagement/Quizzes/BtnSaveAndAddQuestions'))

Helper.waitElementVisible('//span[.=\'Quiz created!\']//parent::div')

WebUI.callTestCase(findTestCase('Studio/Essentials/AssetsManagement/Quizzes/EstlsInputQuiz'), [('mapQuestions') : listMaps], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Studio/General/BtnSave'))