import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.harukaedu.keywords.Helper as Helper

WebUI.callTestCase(findTestCase('Studio/Essentials/CoursesManagement/General/EstlsCoursesAction'), [('courseName') : 'Demo'
        , ('actions') : 'Edit'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Studio/Essentials/CoursesManagement/Courses/EstlsCoursesEdit'), [('code') : '', ('name') : ''
        , ('credit') : '', ('session') : '', ('maxCapacity') : '', ('externalLink') : '', ('discourseLink') : '', ('isSyllabus') : false
        , ('isCourseMaterial') : false, ('isHandout') : false, ('partner') : ''], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Studio/General/BtnSave'))

Helper.waitElementVisible('//span[.=\'Course updated!\']')

