import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.harukaedu.keywords.Helper as Helper

WebUI.click(findTestObject('Studio/AssetsManagement/MenuAssetsManagement'))

WebUI.delay(1)

def menuConv = Helper.capitalizeFully(menu)

switch (menuConv) {
    case 'Videos':
        WebUI.click(findTestObject('Studio/AssetsManagement/Videos/MenuVideos'))

        break
    case 'Audios':
        WebUI.click(findTestObject('Studio/AssetsManagement/Audios/MenuAudios'))

        break
    case 'Files':
        WebUI.click(findTestObject('Studio/AssetsManagement/Files/MenuFiles'))

        break
    case 'Quizzes':
        WebUI.click(findTestObject('Studio/AssetsManagement/Quizzes/MenuQuizzes'))

        break
    case 'H5p':
        WebUI.click(findTestObject('Studio/AssetsManagement/H5P/MenuH5P'))

        break
    default:
        println('Please insert correct menu!')

        break
}