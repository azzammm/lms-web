import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.harukaedu.keywords.Helper as Helper
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

def classNameConvert = Helper.capitalizeFully('Demo')

WebUI.callTestCase(findTestCase('Studio/Essentials/ClassesManagement/General/EstlsClassesAction'), [('className') : classNameConvert
        , ('actions') : 'Manage Gradebooks'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Studio/Essentials/ClassesManagement/GradeBook/EstlsGradeBookInputForm'), [('action') : 'Save'
        , ('isEdit') : true, ('gradebookName') : 'Grade Book From Katalon 1', ('gradebookNameInput') : 'Grade Book From Katalon 1 Edit', ('gradebookPercentage') : '15', ('gradebookType') : 'Post Quiz'
        , ('gradebookExamType') : '', ('gradebookSection') : '', ('gradebookIsAverage') : true, ('gradebookSort') : ''], 
    FailureHandling.STOP_ON_FAILURE)

