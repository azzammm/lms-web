import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.harukaedu.keywords.Helper as Helper
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

def dateMapUnit = [('startDate') : '', ('dueDate') : '']

def className = 'Kelas Aljabar (ALJBR) AAA'

def sectionName = 'Section Create From Katalon'

def unitName = 'PKO-S03-Diskusi Sesi 3'

WebUI.callTestCase(findTestCase('Test Cases/Studio/Essentials/ClassesManagement/General/EstlsSearchClass'), [('className') : className], 
    FailureHandling.STOP_ON_FAILURE)

def dateMapSection = WebUI.callTestCase(findTestCase('Studio/Essentials/ClassesManagement/General/EstlsSectionAction'), 
    [('sectionName') : sectionName, ('action') : 'Add Unit'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Studio/Essentials/ClassesManagement/Unit/EstlsClassesAddUnit'), [('unitName') : unitName], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Studio/Essentials/ClassesManagement/General/EstlsUnitAction'), [('sectionName') : sectionName
        , ('unitName') : unitName, ('action') : 'Edit'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Studio/Essentials/ClassesManagement/Unit/EstlsClassesEditUnit'), [('action') : 'Save', ('startDate') : 0
        , ('dueDate') : 2, ('dueMonth') : 10, ('isClockDueMax') : true], FailureHandling.STOP_ON_FAILURE)

Helper.waitElementVisible("//h2[.='$sectionName']")

textDate = Helper.defineWebElement("//h2[.='$sectionName']/parent::div/following-sibling::ul/li/span[.='$unitName']/following-sibling::p[@class='date']", 
    'xpath').getText()

println(textDate)

def regexResult = textDate =~ '\\b(0[0-9]|[12][0-9]|3[0-2])\\b ([A-Z][a-z])\\w+ (19|20)\\d{2} (00|[0-9]|1[0-9]|2[0-3]):([0-9]|[0-5][0-9])(.|\\n)'

dateMapUnit.size().times({ 
        println((regexResult[it])[0])

        if (it == 0) {
            (dateMapUnit['startDate']) = ((regexResult[it])[0])
        }
        
        if (it == 1) {
            (dateMapUnit['dueDate']) = ((regexResult[it])[0])
        }
    })

println(dateMapSection)

println(dateMapUnit)

WebUI.click(findTestObject('Studio/ClassesManagement/MenuClassesManagement'))

WebUI.callTestCase(findTestCase('Studio/Essentials/ClassesManagement/General/EstlsClassesAction'), [('className') : className
        , ('actions') : 'Preview Learning Activity'], FailureHandling.STOP_ON_FAILURE)

Helper.pickDropdown(findTestObject('Studio/ClassesManagement/PreviewLearningActivity/SlctSection'), '1')

if ((dateMapSection['dueDate']) != (dateMapUnit['dueDate'])) {
    assert Helper.defineWebElement("//a[contains(.,'$unitName')]/span[contains(.,'Start')]", 'xpath').isDisplayed() == 
    true
}