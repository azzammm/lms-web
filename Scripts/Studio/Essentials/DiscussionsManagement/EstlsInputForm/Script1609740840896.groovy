import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.harukaedu.keywords.Helper as Helper
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

if (title != '') {
    WebUI.clearText(findTestObject('Studio/DiscussionsManagement/Discussion/InpTitle'))

    WebUI.sendKeys(findTestObject('Studio/DiscussionsManagement/Discussion/InpTitle'), title)
}

if (description != '') {
    WebUI.clearText(findTestObject('Studio/DiscussionsManagement/Discussion/InpDescription'))

    WebUI.sendKeys(findTestObject('Studio/DiscussionsManagement/Discussion/InpDescription'), description)
}

if (tags != '') {
    WebUI.clearText(findTestObject('Studio/DiscussionsManagement/Discussion/InpTags'))

    WebUI.sendKeys(findTestObject('Studio/DiscussionsManagement/Discussion/InpTags'), tags)
}

isGrouped ? Helper.waitElementAndClick('//label[text()=\'Is Grouped\']/following-sibling::div/label') : false

isLecturesOnly ? Helper.waitElementAndClick('//label[text()=\'Lectures Only\']/following-sibling::div/label') : false

isForum ? Helper.waitElementAndClick('//label[text()=\'Is Forum\']/following-sibling::div/label') : false

isHideDiscussionAnswer ? Helper.waitElementAndClick('//label[text()=\'Hide discussion answers\']/following-sibling::div/label') : false

if (minCharForReply != '') {
    WebUI.clearText(findTestObject('Studio/DiscussionsManagement/Discussion/InpMinCharForReply'))

    WebUI.sendKeys(findTestObject('Studio/DiscussionsManagement/Discussion/InpMinCharForReply'), minCharForReply)
}

if (partner != '') {
    Helper.pickDropdown(findTestObject('Studio/DiscussionsManagement/Discussion/SlctPartner'), partner)
}