import com.harukaedu.keywords.Helper as Helper

Helper.waitElementAndClick("//span[.='$discussionName']//following-sibling::a")

Helper.waitElementAndClick("//span[.='$discussionName']//following-sibling::ul/li/a[.='$action']")

if (action == 'Disable Forum') {
    Helper.driver.switchTo().alert().accept()

    Helper.waitElementVisible('//div/span[.=\'Disable forum successfully\']')
}

if (action == 'Delete') {
    Helper.driver.switchTo().alert().accept()

    Helper.waitElementVisible('//div/span[.=\'Section unit discussion deleted\']')
}

if (action == 'Enable Forum') {
    Helper.driver.switchTo().alert().accept()

    Helper.waitElementVisible('//div/span[.=\'Enable forum successfully\']')
}