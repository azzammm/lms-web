import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.harukaedu.keywords.Helper as Helper
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

if (code != '') {
    WebUI.clearText(findTestObject('Studio/CoursesManagement/Courses/InpCode'))

    WebUI.sendKeys(findTestObject('Studio/CoursesManagement/Courses/InpCode'), code)
}

if (name != '') {
    WebUI.clearText(findTestObject('Studio/CoursesManagement/Courses/InpName'))

    WebUI.sendKeys(findTestObject('Studio/CoursesManagement/Courses/InpName'), name)
}

if (credit != '') {
    WebUI.clearText(findTestObject('Studio/CoursesManagement/Courses/InpCourseCredit'))

    WebUI.sendKeys(findTestObject('Studio/CoursesManagement/Courses/InpCourseCredit'), credit)
}

if (session != '') {
    WebUI.clearText(findTestObject('Studio/CoursesManagement/Courses/InpSession'))

    WebUI.sendKeys(findTestObject('Studio/CoursesManagement/Courses/InpSession'), session)
}

if (maxCapacity != '') {
    WebUI.clearText(findTestObject('Studio/CoursesManagement/Courses/InpMaxCapacity'))

    WebUI.sendKeys(findTestObject('Studio/CoursesManagement/Courses/InpMaxCapacity'), maxCapacity)
}

if (externalLink != '') {
    WebUI.clearText(findTestObject('Studio/CoursesManagement/Courses/InpExternalLink'))

    WebUI.sendKeys(findTestObject('Studio/CoursesManagement/Courses/InpExternalLink'), externalLink)
}

if (discourseLink != '') {
    WebUI.clearText(findTestObject('Studio/CoursesManagement/Courses/InpExternalLink'))

    WebUI.sendKeys(findTestObject('Studio/CoursesManagement/Courses/InpDiscourseLink'), discourseLink)
}

if (isSyllabus == true) {
    WebUI.click(findTestObject('Studio/CoursesManagement/Courses/IsSyllabus'))
}

if (isCourseMaterial == true) {
    WebUI.click(findTestObject('Object Repository/Studio/CoursesManagement/Courses/IsCourseMaterial'))
}

if (isHandout == true) {
    WebUI.click(findTestObject('Object Repository/Studio/CoursesManagement/Courses/IsHandout'))
}

if (partner != '') {
    Helper.pickDropdown(findTestObject('Object Repository/Studio/CoursesManagement/Courses/SlctPartner'), 'Demo')
}