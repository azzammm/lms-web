import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.harukaedu.keywords.Common as Common
import com.harukaedu.keywords.Helper as Helper
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

for (int i = 0; i < mapQuestions.size(); i++) {
    WebUI.click(findTestObject('Studio/AssetsManagement/Quizzes/BtnAddQuiz'))

    Helper.waitElementVisible("//iframe[@title='Rich Text Editor, question_list[$i][question]']")

    def randomNumber = Math.abs(new Random().nextInt() % (mapQuestions[i].answer.size() - 0)) + 0

    def iFrameQuestions = Helper.defineWebElement("//iframe[@title='Rich Text Editor, question_list[$i][question]']", 'xpath')

    Common.driver.switchTo().frame(iFrameQuestions)

    Helper.defineWebElement('//body', 'xpath').sendKeys(mapQuestions[i].question)

    Common.driver.switchTo().parentFrame()

    for (int k = 0; k < mapQuestions[i].answer.size(); k++) {
        Helper.waitElementAndClick("//div[contains(@class,'question-$i')]//span[@class='add-option'][normalize-space()='Add Answer']")

        Helper.waitElementVisible("//iframe[@title='Rich Text Editor, question_list[$i][question_options][$k][option]']")

        def iFrameAnswers = Helper.defineWebElement("//iframe[@title='Rich Text Editor, question_list[$i][question_options][$k][option]']", 
            'xpath')

        Common.driver.switchTo().frame(iFrameAnswers)

        Helper.defineWebElement('//body', 'xpath').sendKeys(mapQuestions[i].answer[k])

        Common.driver.switchTo().parentFrame()
    }
    
    Helper.defineWebElement("//input[@name='question_list[$i][question_options][$randomNumber][is_answer]']", 'xpath').click()

    WebUI.scrollToPosition(0, 0)
}