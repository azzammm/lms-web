import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.harukaedu.keywords.Helper as Helper
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

WebUI.click(findTestObject('Object Repository/Studio/ObjectivesManagement/MenuObjectivesManagement'))

WebUI.click(findTestObject('Object Repository/Studio/General/BtnAddRed'))

WebUI.callTestCase(findTestCase('Studio/Essentials/ObjectivesManagement/EstlsInputForm'), [('key') : 'Objective from Katalon'
        , ('value') : 'Objective from Katalon', ('partner') : 'Demo'], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Studio/General/BtnSave'))

Helper.waitElementVisible('//span[.=\'Objective created!\']//parent::div')