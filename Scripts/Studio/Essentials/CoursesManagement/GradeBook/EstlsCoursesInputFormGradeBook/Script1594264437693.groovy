import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.harukaedu.keywords.Helper as Helper
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

if (gradeBookName != '') {
    WebUI.clearText(findTestObject('Studio/CoursesManagement/GradeBook/InpName'))

    WebUI.sendKeys(findTestObject('Studio/CoursesManagement/GradeBook/InpName'), gradeBookName)
}

if (percentage != '') {
    WebUI.clearText(findTestObject('Studio/CoursesManagement/GradeBook/InpPercentage'))

    WebUI.sendKeys(findTestObject('Studio/CoursesManagement/GradeBook/InpPercentage'), percentage)
}

if (gradeBookType != '') {
    Helper.pickDropdown(findTestObject('Object Repository/Studio/CoursesManagement/GradeBook/SlctGradebookType'), gradeBookType)
}

if (examType != '') {
    Helper.pickDropdown(findTestObject('Object Repository/Studio/CoursesManagement/GradeBook/SlctExamType'), examType)

    if (sectionType != '') {
        Helper.pickDropdown(findTestObject('Object Repository/Studio/CoursesManagement/GradeBook/SlctSection'), sectionType)
    }
}

if (isAverage == true) {
    WebUI.click(findTestObject('Object Repository/Studio/CoursesManagement/GradeBook/RbtnAverageYes'))
}

if (isAverage == false) {
    WebUI.click(findTestObject('Object Repository/Studio/CoursesManagement/GradeBook/RbtnAverageNo'))
}

if (sort != '') {
    WebUI.sendKeys(findTestObject('Studio/CoursesManagement/GradeBook/InpSort'), sort)
}