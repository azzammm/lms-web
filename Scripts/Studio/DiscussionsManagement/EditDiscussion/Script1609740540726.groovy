import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.harukaedu.keywords.Helper as Helper
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

WebUI.click(findTestObject('Studio/DiscussionsManagement/Discussion/MenuDiscussionsManagement'))

WebUI.callTestCase(findTestCase('Studio/Essentials/DiscussionsManagement/EstlsUnitAction'), [('unitName') : 'Discussion from Katalon'
        , ('actions') : 'Edit'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Studio/Essentials/DiscussionsManagement/EstlsInputForm'), [('title') : 'Discussion from Katalon Edit'
        , ('description') : 'Discussion from Katalon Edit', ('tags') : 'tech, test, qa', ('minCharForReply') : '10', ('partner') : 'Demo'], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Studio/General/BtnSave'))

Helper.waitElementVisible('//span[.=\'Discussion updated!\']//parent::div')