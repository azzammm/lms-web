import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import com.harukaedu.keywords.Helper as Helper
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

sectionName = 'Section Create From Katalon'

unitName = 'PKO-S03-03-Output'

dateMapUnit = [('startDate') : '', ('dueDate') : '']

def regex = '\\b(0[0-9]|[12][0-9]|3[0-2])\\b ([A-Z][a-z])\\w+ (19|20)\\d{2} (00|[0-9]|1[0-9]|2[0-3]):([0-9]|[0-5][0-9])(.|\\n)'

WebUI.callTestCase(findTestCase('Studio/Essentials/ClassesManagement/General/EstlsSearchClass'), [('className') : 'Kelas Aljabar (ALJBR) AAA'], 
    FailureHandling.STOP_ON_FAILURE)

def dateMapSection = WebUI.callTestCase(findTestCase('Studio/Essentials/ClassesManagement/General/EstlsSectionAction'), 
    [('sectionName') : sectionName, ('action') : 'Add Unit'], FailureHandling.STOP_ON_FAILURE)

println(dateMapSection)

WebUI.callTestCase(findTestCase('Studio/Essentials/ClassesManagement/Unit/EstlsClassesAddUnit'), [('unitName') : unitName], 
    FailureHandling.STOP_ON_FAILURE)

Helper.waitElementVisible("//h2[.='$sectionName']")

textDate = Helper.defineWebElement("//h2[.='$sectionName']/parent::div/following-sibling::ul/li/span[.='$unitName']/following-sibling::p[@class='date']", 
    'xpath').getText()

def regexResult = textDate =~ regex

dateMapUnit.size().times({ 
        if (it == 0) {
            (dateMapUnit['startDate']) = ((regexResult[it])[0])
        }
        
        if (it == 1) {
            (dateMapUnit['dueDate']) = ((regexResult[it])[0])
        }
    })

assert (dateMapSection['startDate']) == (dateMapUnit['startDate'])

assert (dateMapSection['dueDate']) == (dateMapUnit['dueDate'])