import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.harukaedu.keywords.Helper as Helper
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

WebUI.waitForElementVisible(findTestObject('General/BtnBurgerMenu'), 10)

courseName = Helper.defineWebElement('//a[contains(text(),\'Kelas Germany Languange (JRM) C\')]', 'xpath')

Helper.waitElementAndClick(courseName)

WebUI.delay(3)

Helper.defineWebElement('//div[@class=\'select-wrapper section-sort width-sort-learning-activity\']//input[@class=\'select-dropdown\']', 
    'xpath').click()

WebUI.delay(1)

Helper.defineWebElement('//ul//span[contains(text(),\'6\')]', 'xpath').click()

Helper.defineWebElement('//a[contains(text(),\'Post Quiz Azzam\')]', 'xpath').click()

Helper.defineWebElement('//a[contains(text(),\'Take Quiz\')]', 'xpath').click()

questions = Helper.defineWebElements('//div[contains(@class,\'question-list\')]/div', 'xpath')

answerA = Helper.defineWebElements('//div[contains(@class,\'question\')]//span[contains(text(),\'A\')]', 'xpath')

btnNext = Helper.defineWebElement('//a[contains(text(),\'next\')]', 'xpath')

for (int i = 0; i <= questions.size(); i++) {
    if ((i + 1) == questions.size()) {
        WebUI.click(findTestObject('Student/Dashboard/Quiz/BtnSubmitQuiz'))

        break
    }
    
    (answerA[i]).click()

    btnNext.click()

    WebUI.delay(2)
}

WebUI.verifyTextPresent('Wrong', false)

WebUI.verifyTextPresent('Correct', false)

assert Helper.defineWebElements('//strong/div[@class=\'row\']', 'xpath').size() == questions.size()