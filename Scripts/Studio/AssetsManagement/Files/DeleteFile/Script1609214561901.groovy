import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import com.harukaedu.keywords.Helper as Helper
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

WebUI.callTestCase(findTestCase('Studio/Essentials/AssetsManagement/EstlsDirectMenuAssets'), [('menu') : 'files'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Studio/Essentials/AssetsManagement/Files/EstlsUnitAction'), [('unitName') : 'Files from Katalon Edit'
        , ('actions') : 'Delete'], FailureHandling.STOP_ON_FAILURE)

Helper.waitElementVisible('//span[.=\'File deleted\']//parent::div')