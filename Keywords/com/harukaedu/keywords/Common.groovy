package com.harukaedu.keywords

import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.openqa.selenium.interactions.Actions
import org.openqa.selenium.support.ui.WebDriverWait

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.configuration.RunConfiguration
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webui.common.WebUiCommonHelper
import com.kms.katalon.core.webui.driver.DriverFactory

public class Common {

	public static WebDriver driver
	public static WebDriverWait wait
	public static Actions actions
	public static String path

	/**
	 * Base variable of Helper
	 */
	@Keyword
	public static void setUp(){
		driver = DriverFactory.getWebDriver()
		wait = new WebDriverWait(driver, 10)
		actions = new Actions(driver)
		path = RunConfiguration.getProjectDir().concat('/Data Files/')
	}

	public static WebElement findWebElement(TestObject to){
		return WebUiCommonHelper.findWebElement(to,10)
	}
}
