import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.harukaedu.keywords.Helper as Helper
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

if (sectionName != '') {
    WebUI.clearText(findTestObject('Studio/CoursesManagement/Section/InpSectionName'))

    WebUI.sendKeys(findTestObject('Studio/CoursesManagement/Section/InpSectionName'), sectionName)
}

if (description != '') {
    WebUI.clearText(findTestObject('Studio/CoursesManagement/Section/InpDescription'))

    WebUI.sendKeys(findTestObject('Studio/CoursesManagement/Section/InpDescription'), description)
}

if (startDate > 0) {
	Helper.pickDate(findTestObject('Object Repository/Studio/CoursesManagement/Section/InpStartDate'), startDate, startDateMonth, startDateYear)

	WebUI.delay(1)
}

if (dueDate > 0) {
    Helper.pickDate(findTestObject('Object Repository/Studio/CoursesManagement/Section/InpDueDate'), dueDate, dueDateMonth, dueDateYear)

    WebUI.delay(1)
}

if (endDate > 0) {
    Helper.pickDate(findTestObject('Object Repository/Studio/CoursesManagement/Section/InpEndDate'), endDate, endDateMonth, endDateYear)
	
	WebUI.delay(1)
}

if (isAttendance) {
    WebUI.click(findTestObject('Studio/CoursesManagement/Section/ChbxIsOfflineClass'))
}

if (sort != '') {
    WebUI.clearText(findTestObject('Studio/CoursesManagement/Section/InpSort'))

    WebUI.sendKeys(findTestObject('Studio/CoursesManagement/Section/InpSort'), sort)
}

if (partnerName != '') {
    Helper.pickDropdown(findTestObject('Studio/CoursesManagement/Section/SlctPartner'), partnerName)
}