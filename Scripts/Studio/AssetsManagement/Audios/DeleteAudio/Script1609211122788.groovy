import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import com.harukaedu.keywords.Helper as Helper
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

WebUI.callTestCase(findTestCase('Studio/Essentials/AssetsManagement/EstlsDirectMenuAssets'), [('menu') : 'audios'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Studio/Essentials/AssetsManagement/Audios/EstlsUnitAction'), [('unitName') : 'Audio from Katalon Edit'
        , ('actions') : 'Delete'], FailureHandling.STOP_ON_FAILURE)

Helper.waitElementVisible('//span[.=\'Audio deleted\']//parent::div')