import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase

import com.kms.katalon.core.annotation.AfterTestCase
import com.kms.katalon.core.annotation.BeforeTestCase
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

class Listener {

	@BeforeTestCase
	def beforeTestCase() {
		WebUI.callTestCase(findTestCase('Basic/Login'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@AfterTestCase
	def afterTestCase() {
		WebUI.takeScreenshot()
		WebUI.closeBrowser()
	}
}