import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

listMaps = [[('question') : 'udah makan belum? ini versi tambahan', ('answer') : ['udin dings', 'mantap, udah dong']], [
        ('question') : 'udah minum belum? ini versi tambahan', ('answer') : ['Sudah lagii deh gue', 'Belum lagii dah gue']]]

WebUI.callTestCase(findTestCase('Studio/Essentials/AssetsManagement/EstlsDirectMenuAssets'), [('menu') : 'quizzes'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Studio/Essentials/AssetsManagement/Quizzes/EstlsUnitAction'), [('unitName') : 'Quiz from Katalon Edit'
        , ('actions') : 'Edit'], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Studio/AssetsManagement/Quizzes/BtnQuestion'))

WebUI.click(findTestObject('Studio/AssetsManagement/Quizzes/BtnAddQuestion'))

WebUI.callTestCase(findTestCase('Studio/Essentials/AssetsManagement/Quizzes/EstlsInputQuiz'), [('mapQuestions') : listMaps], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Studio/General/BtnSave'))