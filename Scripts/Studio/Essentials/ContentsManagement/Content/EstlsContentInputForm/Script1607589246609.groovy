import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.harukaedu.keywords.Common as Common
import com.harukaedu.keywords.Helper as Helper
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

if (title != '') {
    WebUI.clearText(findTestObject('Object Repository/Studio/ContentsManagement/Content/InpTitle'))

    WebUI.sendKeys(findTestObject('Object Repository/Studio/ContentsManagement/Content/InpTitle'), title)
}

if (description != '') {
    WebUI.clearText(findTestObject('Object Repository/Studio/ContentsManagement/Content/TxtDescription'))

    WebUI.sendKeys(findTestObject('Object Repository/Studio/ContentsManagement/Content/TxtDescription'), description)
}

if (tags != '') {
	WebUI.clearText(findTestObject('Object Repository/Studio/ContentsManagement/Content/InpTags'))
	
	WebUI.sendKeys(findTestObject('Object Repository/Studio/ContentsManagement/Content/InpTags'), tags)
}

if (contentType != '') {
    WebUI.click(findTestObject('Object Repository/Studio/ContentsManagement/Content/SlctContentType'))

    def object = Helper.defineWebElement("//span[contains(text(),'$contentType')]//parent::li", 'xpath')

    Common.actions.moveToElement(object)

    Common.actions.perform()

    object.click()
}


if (topic != '') {
    WebUI.clearText(findTestObject('Object Repository/Studio/ContentsManagement/Content/InpTopic'))

    WebUI.sendKeys(findTestObject('Object Repository/Studio/ContentsManagement/Content/InpTopic'), topic)
}

if (objective != '') {
    WebUI.clearText(findTestObject('Object Repository/Studio/ContentsManagement/Content/InpObjective'))

    WebUI.sendKeys(findTestObject('Object Repository/Studio/ContentsManagement/Content/InpObjective'), objective)
}

if (attempts != '') {
	WebUI.click(findTestObject('Object Repository/Studio/ContentsManagement/Content/SlctAttempts'))

	def object = Helper.defineWebElement("//span[contains(text(),'$attempts')]//parent::li", 'xpath')

	Common.actions.moveToElement(object)

	Common.actions.perform()

	object.click()
}

if (partner != '') {
    Helper.pickDropdown(findTestObject('Object Repository/Studio/ContentsManagement/Content/SlctPartner'), partner)
}