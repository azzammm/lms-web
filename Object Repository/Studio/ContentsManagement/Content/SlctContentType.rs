<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>SlctContentType</name>
   <tag></tag>
   <elementGuidId>32bb9625-3e5f-4cd7-b51f-746edc3975ce</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//label[.='Content Type']/parent::div</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
