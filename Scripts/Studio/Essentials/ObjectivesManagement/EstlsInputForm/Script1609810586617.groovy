import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.harukaedu.keywords.Common
import com.harukaedu.keywords.Helper
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

if(key != ""){
	WebUI.clearText(findTestObject('Studio/ObjectivesManagement/InpKey'))
	
	WebUI.sendKeys(findTestObject('Studio/ObjectivesManagement/InpKey'), key)
}

if(value != ""){
	iFrame = Helper.defineWebElement('//iframe[@title=\'Rich Text Editor, value\']', 'xpath')

    Common.driver.switchTo().frame(iFrame)

	Helper.waitElementVisible("//body")
	
    bodyPost = Helper.defineWebElement('//body', 'xpath')

    bodyPost.clear()

    bodyPost.sendKeys(value)

    Common.driver.switchTo().parentFrame()
}

if(partner != ""){
	Helper.pickDropdown(findTestObject('Studio/General/SlctPartner'), partner)
}