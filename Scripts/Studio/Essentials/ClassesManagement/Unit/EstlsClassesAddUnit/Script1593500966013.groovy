import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.harukaedu.keywords.Common as Common
import com.harukaedu.keywords.Helper as Helper
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

WebUI.clearText(findTestObject('Studio/General/InpSearch'))

WebUI.sendKeys(findTestObject('Studio/General/InpSearch'), unitName)

WebUI.click(findTestObject('Studio/General/BtnFilter'))

Helper.waitElementVisible("//a[.='$unitName']")

def card = Helper.defineWebElement("//a[.='$unitName']", 'xpath')

def checklistCard = Helper.defineWebElement(card, '//parent::div/following-sibling::div//label', 'xpath')

checklistCard.click()

WebUI.delay(1)

WebUI.click(findTestObject('Studio/General/BtnSave'))

Helper.waitElementVisible('//div[@class=\'notif-container\']')