import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.harukaedu.keywords.Helper

WebUI.click(findTestObject('General/BtnBurgerMenu'))

WebUI.click(findTestObject('Student/Dashboard/News/MenuNews'))

assert WebUI.getWindowTitle() == 'LMS - News'

assert Helper.defineWebElement("//h2[contains(.,'LMS - News')]", "xpath").isDisplayed() == true