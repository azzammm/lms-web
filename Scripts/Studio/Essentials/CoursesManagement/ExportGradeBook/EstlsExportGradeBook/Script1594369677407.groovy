import com.harukaedu.keywords.Helper as Helper
import com.harukaedu.keywords.Common as Common
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

def listCard = Helper.defineWebElements('//div[contains(@class,\'klass-group group\')]/div//span/a', 'xpath')

def listCardName = []

def cardUncheck = []

if (listCard.size() > 0) {
    listCard.size().times({ 
            listCardName.add((listCard[it]).getText())
        })

    if (listCard.size() > 10) {
        def btnNext = Helper.defineWebElement('//a[@id=\'btn_next\']//i[@class=\'material-icons\'][contains(text(),\'chevron_right\')]', 
            'xpath')

        Common.actions.moveToElement(btnNext).perform()

        btnNext.click()

        listCard = Helper.defineWebElements('//div[contains(@class,\'klass-group group\')]/div//span/a', 'xpath')

        listCard.size().times({ 
                listCardName.add((listCard[it]).getText())
            })

        def btnBack = Helper.defineWebElement('//a[@id=\'btn_prev\']//i[@class=\'material-icons\'][contains(text(),\'chevron_left\')]', 
            'xpath')

        btnBack.click()
    }
    
    listCardName.removeAll(Arrays.asList('', null))
}

listCardName.size().times({ 
        println(it)

        def card = listCardName[it]

        println(card)

        if (!(listCardName.contains(classNameList[it]))) {
            if (it == 10) {
                def btnNext = Helper.defineWebElement('//a[@id=\'btn_next\']//i[@class=\'material-icons\'][contains(text(),\'chevron_right\')]', 
                    'xpath')

                Common.actions.moveToElement(btnNext).perform()

                btnNext.click()
            }
            
            def itemChecklist = Helper.defineWebElement("//a[.='$card']//parent::span//parent::div//following-sibling::div/p/label", 
                'xpath')

            Common.actions.moveToElement(itemChecklist).perform()

            itemChecklist.click()
        }
    })