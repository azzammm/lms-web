import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.harukaedu.keywords.Helper

WebUI.waitForElementVisible(findTestObject('Studio/General/BtnFilter'), 10)

WebUI.click(findTestObject('Studio/CoursesManagement/MenuCoursesManagement'))

WebUI.waitForElementVisible(findTestObject('Studio/General/BtnFilter'), 10)

WebUI.click(findTestObject('Studio/UnitsManagement/MenuUnitsManagement'))

WebUI.click(findTestObject('Studio/ContentsManagement/MenuContentsManagement'))

WebUI.click(findTestObject('Studio/AssetsManagement/MenuAssetsManagement'))

WebUI.click(findTestObject('Studio/AssetsManagement/Videos/MenuVideos'))

WebUI.waitForElementVisible(findTestObject('Studio/General/BtnSearch'), 10)

WebUI.click(findTestObject('Studio/AssetsManagement/Audios/MenuAudios'))

WebUI.waitForElementVisible(findTestObject('Studio/General/BtnSearch'), 10)

WebUI.click(findTestObject('Studio/AssetsManagement/Files/MenuFiles'))

WebUI.waitForElementVisible(findTestObject('Studio/General/BtnSearch'), 10)

WebUI.click(findTestObject('Studio/AssetsManagement/Quizzes/MenuQuizzes'))

WebUI.waitForElementVisible(findTestObject('Studio/General/BtnSearch'), 10)

WebUI.click(findTestObject('Studio/AssetsManagement/H5P/MenuH5P'))

WebUI.waitForElementVisible(findTestObject('Studio/General/BtnSearch'), 10)

WebUI.click(findTestObject('Studio/DiscussionsManagement/Discussion/MenuDiscussionsManagement'))

WebUI.waitForElementVisible(findTestObject('Studio/General/BtnSearch'), 10)

WebUI.click(findTestObject('Studio/TopicsManagement/MenuTopicsManagement'))

WebUI.waitForElementVisible(findTestObject('Studio/General/BtnSearch'), 10)

WebUI.click(findTestObject('Studio/ObjectivesManagement/MenuObjectivesManagement'))

WebUI.waitForElementVisible(findTestObject('Studio/General/BtnSearch'), 10)

WebUI.click(findTestObject('Studio/NotificationManagement/General/MenuNotificationManagement'))

WebUI.waitForElementVisible(findTestObject('Studio/General/BtnSearch'), 10)

WebUI.click(findTestObject('Studio/ScheduleManagement/MenuScheduleManagement'))

WebUI.waitForElementVisible(findTestObject('Studio/General/BtnSearch'), 10)

WebUI.click(findTestObject('Studio/UsersManagement/SubmenuUserManagement'))

WebUI.click(findTestObject('Studio/UsersManagement/Users/SubmenuUsers'))

Helper.waitElementVisible("//h2[text()='Users']")

WebUI.click(findTestObject('Studio/UsersManagement/Students/MenuStudents'))

Helper.waitElementVisible("//h2[text()='Students']")

WebUI.click(findTestObject('Studio/ScheduleManagement/MenuScheduleManagement'))

WebUI.click(findTestObject('Studio/RecalculateQuiz/MenuRecalculateQuiz'))

Helper.waitElementVisible("//h2[text()='Recalculate Quiz']")