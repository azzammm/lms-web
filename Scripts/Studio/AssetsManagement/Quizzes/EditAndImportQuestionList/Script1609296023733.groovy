import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.harukaedu.keywords.Helper
WebUI.callTestCase(findTestCase('Studio/Essentials/AssetsManagement/EstlsDirectMenuAssets'), [('menu') : 'quizzes'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Studio/Essentials/AssetsManagement/Quizzes/EstlsUnitAction'), [('unitName') : 'Quiz from Katalon Edit'
        , ('actions') : 'Edit'], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Studio/AssetsManagement/Quizzes/BtnQuestion'))

WebUI.click(findTestObject('Studio/AssetsManagement/Quizzes/BtnImport'))

Helper.uploadFile(findTestObject('Studio/AssetsManagement/Quizzes/UplFile'), "import_quiz.csv")

WebUI.click(findTestObject('Studio/AssetsManagement/Quizzes/BtnImportUploadFile'))

WebUI.click(findTestObject('Studio/AssetsManagement/Quizzes/BtnSaveImport'))

Helper.waitElementVisible('//span[.=\'Import Success!\']//parent::div')