import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import com.harukaedu.keywords.Common as Common
import com.harukaedu.keywords.Helper as Helper
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

def className = 'Kelas Aljabar (ALJBR) L'

WebUI.callTestCase(findTestCase('Studio/Essentials/ClassesManagement/General/EstlsSearchClass'), [('className') : className], 
    FailureHandling.STOP_ON_FAILURE)

def totalData = Helper.defineWebElements('//button[contains(text(),\'Delete\')]', 'xpath').size()

for (int i = 0; i < totalData; i++) {
    def btnAction = Helper.defineWebElements('//button[contains(text(),\'Delete\')]', 'xpath')

    Helper.waitElementVisible(btnAction[0])

    Common.actions.moveToElement(btnAction[0]).perform()

    (btnAction[0]).click()

    WebUI.acceptAlert()

    Helper.waitElementVisible('//div[@class=\'card-panel teal lighten-2\']')
}