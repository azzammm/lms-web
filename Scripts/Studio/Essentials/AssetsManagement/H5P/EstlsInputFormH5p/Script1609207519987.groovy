import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.harukaedu.keywords.Helper as Helper
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

if (title != '') {
    WebUI.clearText(findTestObject('Studio/AssetsManagement/H5P/InpTitle'))

    WebUI.sendKeys(findTestObject('Studio/AssetsManagement/H5P/InpTitle'), title)
}

if (description != '') {
    WebUI.clearText(findTestObject('Studio/AssetsManagement/H5P/InpDescription'))

    WebUI.sendKeys(findTestObject('Studio/AssetsManagement/H5P/InpDescription'), description)
}

if (file != '') {
    Helper.uploadFile(findTestObject('Studio/AssetsManagement/H5P/UplFile'), file)
}

if (tags != '') {
    WebUI.clearText(findTestObject('Studio/AssetsManagement/H5P/InpTags'))

    WebUI.sendKeys(findTestObject('Studio/AssetsManagement/H5P/InpTags'), tags)
}

if (partner != '') {
    Helper.pickDropdown(findTestObject('Studio/AssetsManagement/H5P/SlctPartner'), partner)
}