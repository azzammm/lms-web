import com.harukaedu.keywords.Common as Common
import com.harukaedu.keywords.Helper as Helper

def dateMap = [('startDate') : '', ('endDate') : '', ('dueDate') : '']

def btnAction

def convertedActions = Helper.capitalizeFully(actions)

Helper.waitElementVisible("//h2[contains(.,'$sectionName')]")

if (actions == 'Delete') {
    btnAction = Helper.defineWebElement("//h2[.='$sectionName']/parent::div/following-sibling::div[@class='card-action']/button", 
        'xpath')

    Common.actions.moveToElement(btnAction).perform()

    btnAction.click()

    Helper.driver.switchTo().alert().accept()

    Helper.waitElementVisible('//div[@class=\'card-panel teal lighten-2\']')
} else {
    btnAction = Helper.defineWebElement("//h2[.='$sectionName']/parent::div/following-sibling::div[@class='card-action']/a[contains(.,'$convertedActions')]", 
        'xpath')

    textDate = Helper.defineWebElement("//h2[.='$sectionName']//parent::div/following-sibling::p[@class='date']", 'xpath').getText()

    def regexResult = textDate =~ '\\b(0[0-9]|[12][0-9]|3[0-2])\\b ([A-Z][a-z])\\w+ (19|20)\\d{2} (00|[0-9]|1[0-9]|2[0-3]):([0-9]|[0-5][0-9])(.|\\n)'

    dateMap.size().times({ 
            if (it == 0) {
                (dateMap['startDate']) = ((regexResult[it])[0])
            }
            
            if (it == 1) {
                (dateMap['endDate']) = ((regexResult[it])[0])
            }
            
            if (it == 2) {
                (dateMap['dueDate']) = ((regexResult[it])[0])
            }
        })

    Common.actions.moveToElement(btnAction).perform()

    btnAction.click()
	
	return dateMap
}