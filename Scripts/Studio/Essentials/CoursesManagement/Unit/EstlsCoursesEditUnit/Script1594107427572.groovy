import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.harukaedu.keywords.Helper as Helper
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

if (title != '') {
    WebUI.clearText(findTestObject('Studio/CoursesManagement/Unit/InpTitle'))

    WebUI.sendKeys(findTestObject('Studio/CoursesManagement/Unit/InpTitle'), title)
}

if (description != '') {
    WebUI.sendKeys(findTestObject('Studio/CoursesManagement/Unit/TxtAreaDescription'), description)
}

if (startDate > 0) {
    Helper.pickDate(findTestObject('Studio/CoursesManagement/Unit/InpStartDate'), startDate, 9, 2020, false)

    WebUI.delay(1)
}

if (dueDate > 0) {
    Helper.pickDate(findTestObject('Studio/CoursesManagement/Unit/InpDueDate'), dueDate, 7, 2021, true)

    WebUI.delay(1)
}

if (sort != '') {
    WebUI.clearText(findTestObject('Studio/CoursesManagement/Unit/InpSort'))

    WebUI.sendKeys(findTestObject('Studio/CoursesManagement/Unit/InpSort'), sort)
}

if (attempts != '') {
    Helper.pickDropdown(findTestObject('Studio/CoursesManagement/Unit/SlctAttempts'), attempts)
}

if (tags != '') {
    WebUI.clearText(findTestObject('Studio/CoursesManagement/Unit/InpTags'))

    WebUI.sendKeys(findTestObject('Studio/CoursesManagement/Unit/InpTags'), tags)
}

if (topic != '') {
    Helper.pickDropdown(findTestObject('Studio/CoursesManagement/Unit/SlctTopic'), topic)
}

if (objective != '') {
    Helper.pickDropdown(findTestObject('Studio/CoursesManagement/Unit/SlctObjective'), objective)
}

if (partner != '') {
    Helper.pickDropdown(findTestObject('Studio/CoursesManagement/Unit/SlctPartner'), partner)
}

WebUI.click(findTestObject('Studio/General/BtnSave'))