import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.harukaedu.keywords.Helper as Helper
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

if (partner != '') {
    Helper.pickDropdown(findTestObject('Object Repository/Studio/NotificationManagement/Form/SlctPartner'), partner)
}

if (recipients != '') {
    Helper.pickDropdown(findTestObject('Object Repository/Studio/NotificationManagement/Form/SlctRecipients'), recipients)
}

if (title != '') {
    WebUI.clearText(findTestObject('Object Repository/Studio/NotificationManagement/Form/InpTitle'))

    WebUI.sendKeys(findTestObject('Object Repository/Studio/NotificationManagement/Form/InpTitle'), title)
}

if (body != '') {
    WebUI.clearText(findTestObject('Object Repository/Studio/NotificationManagement/Form/InpBody'))

    WebUI.sendKeys(findTestObject('Object Repository/Studio/NotificationManagement/Form/InpBody'), body)
}

if (url != '') {
    WebUI.clearText(findTestObject('Object Repository/Studio/NotificationManagement/Form/InpUrl'))

    WebUI.sendKeys(findTestObject('Object Repository/Studio/NotificationManagement/Form/InpUrl'), url)
}

if ((isPushNotification == true) && (Helper.defineWebElement('deliver_at', 'id').isDisplayed() == true)) {
    Helper.pickDateTime(findTestObject('Object Repository/Studio/NotificationManagement/Form/InpClndrDeliveryAt'), dateDelivery, 
        monthDelivery, yearDelivery, isMaxHours)
}

if ((isPushNotification == true) && (Helper.defineWebElement('deliver_at', 'id').isDisplayed() == false)) {
    WebUI.click(findTestObject('Object Repository/Studio/NotificationManagement/Form/SliderPushNotification'))

    Helper.pickDateTime(findTestObject('Object Repository/Studio/NotificationManagement/Form/InpClndrDeliveryAt'), dateDelivery, 
        monthDelivery, yearDelivery, isMaxHours)
}

if ((isPushNotification == false) && (Helper.defineWebElement('deliver_at', 'id').isDisplayed() == true)) {
    WebUI.click(findTestObject('Object Repository/Studio/NotificationManagement/Form/SliderPushNotification'))
}