package com.harukaedu.keywords

import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.openqa.selenium.By
import org.openqa.selenium.WebElement
import org.openqa.selenium.support.ui.ExpectedConditions
import org.openqa.selenium.support.ui.Select

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI


public class Helper extends Common {

	@Keyword
	def static void addGlobalVariable(String name, def value) {
		MetaClass mc = script.evaluate("internal.GlobalVariable").metaClass
		String getterName = "get" + name.capitalize()
		mc.static."$getterName" = { -> return value }
	}

	@Keyword
	def static String capitalizeFully(String str) {
		StringBuilder sb = new StringBuilder();
		boolean cnl = true; // <-- capitalize next letter.
		for (char c : str.toCharArray()) {
			if (cnl && Character.isLetter(c)) {
				sb.append(Character.toUpperCase(c));
				cnl = false;
			} else {
				sb.append(Character.toLowerCase(c));
			}
			if (Character.isWhitespace(c)) {
				cnl = true;
			}
		}
		return sb.toString();
	}

	@Keyword
	def static TestObject declareObject(String xpath) {
		TestObject to = findTestObject('General/GeneralObject')
		to.findProperty('xpath').setValue(xpath)
		return to
	}

	/**
	 * 
	 * @param to
	 * @param file
	 * @author Azzam
	 */

	@Keyword
	public static void uploadFile(TestObject to, String file) {
//		if(System.getProperty('os.name').toString().concat('Windows')){
//			path = path.replace('/','\\')
//		}
		WebUI.uploadFile(to, path+file)
	}

	/**
	 * Find web element based on test object
	 *
	 * @param object TestObject on object repository
	 * @return Return WebElement
	 */
	@Keyword
	public static void waitUntilElementNotVisible(TestObject to) {
		def element = findWebElement(to)
		wait.until(ExpectedConditions.invisibilityOf(element))
	}

	/**
	 * Wait web element on browser and click it
	 * 
	 * @param xPath String XPath Web Element
	 * @throws IOException throws error
	 */
	@Keyword
	public static void waitElementAndClick(String xPath) throws IOException {
		WebElement object = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(xPath)))
		object.click()
	}

	/**
	 * Wait web element on browser and click it
	 *
	 * @param xPath String Web Element
	 * @throws IOException throws error
	 */
	@Keyword
	public static void waitElementAndClick(WebElement webElement) throws IOException {
		WebElement object = wait.until(ExpectedConditions.elementToBeClickable(webElement))
		object.click()
	}

	/**
	 * 
	 * @param xpath
	 * @throws IOException
	 */
	public static void waitElementVisible(String xpath) throws IOException {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)))
	}

	/**
	 * 
	 * @param webElement
	 * @throws IOException
	 */
	public static void waitElementVisible(WebElement webElement) throws IOException {
		wait.until(ExpectedConditions.visibilityOf(webElement))
	}

	/**
	 * Pick your date automatically on calendar
	 *
	 * @param to Test Object
	 * @param date your date on calendar
	 * @param month your month on calendar
	 * @param year your year on calendar
	 * @param max is max value for your clock?
	 * @author Azzam
	 */
	@Keyword
	public static void pickDateTime(TestObject to, int date, int month, int year, boolean max) {
		def field = findWebElement(to)
		field.click()
		def dateInput
		def monthInput
		def realMonth
		def yearInput

		if(month > 0){
			realMonth = month-1
		} else {
			def currentMonth = new Date().format('M')
			realMonth = Integer.parseInt(currentMonth)-1
		}

		if(year > 0 ){
			2.times{
				defineWebElement("//div[@class='datepicker -bottom-left- -from-bottom- active']//div[@class='datepicker--nav-title']", 'xpath').click()
			}

			yearInput = defineWebElement("//div[@class='datepicker -bottom-left- -from-bottom- active']//div[@class='datepicker--cells datepicker--cells-years']//div[@data-year='$year']", 'xpath')

			actions.moveToElement(yearInput)

			actions.perform()

			yearInput.click()
		} else {
			defineWebElement("//div[@class='datepicker -bottom-left- -from-bottom- active']//div[@class='datepicker--nav-title']", 'xpath').click()
		}

		monthInput = defineWebElement("//div[@class='datepicker -bottom-left- -from-bottom- active']//div[@class='datepicker--cells datepicker--cells-months']/div[@data-month='$realMonth']", "xpath")

		actions.moveToElement(monthInput)

		actions.perform()

		monthInput.click()

		dateInput = defineWebElement("//div[@class='datepicker -bottom-left- -from-bottom- active']//div[contains(@class,'datepicker--cell datepicker--cell-day')][@data-date='$date'][@data-month='$realMonth']", "xpath")

		actions.moveToElement(dateInput)

		actions.perform()

		dateInput.click()

		def sliderHours = defineWebElement("//div[@class='datepicker -bottom-left- -from-bottom- active']//div[@class='datepicker--time-sliders']//div[1]", 'xpath')

		def hoursInput = defineWebElement("//div[@class='datepicker -bottom-left- -from-bottom- active']//div[@class='datepicker--time-sliders']//div[1]/input", 'xpath')

		int xHoursCoord =  hoursInput.getLocation().getX()

		int sliderHoursSize = sliderHours.getSize().getWidth()

		def minutesInput = defineWebElement("//div[@class='datepicker -bottom-left- -from-bottom- active']//div[@class='datepicker--time-sliders']//div[2]/input", 'xpath')

		def sliderMinutes = defineWebElement("//div[@class='datepicker -bottom-left- -from-bottom- active']//div[@class='datepicker--time-sliders']//div[2]", 'xpath')

		int xMinutesCoord =  minutesInput.getLocation().getX()

		int sliderMinutesSize = sliderMinutes.getSize().getWidth()

		if(max == true){
			actions.moveToElement(hoursInput).click().dragAndDropBy(hoursInput, xHoursCoord+sliderHoursSize, 0).build().perform()
			WebUI.delay(1)
			actions.moveToElement(minutesInput).click().dragAndDropBy(minutesInput, xMinutesCoord+sliderMinutesSize, 0).build().perform()

		} else {
			actions.moveToElement(hoursInput).click().dragAndDropBy(hoursInput, sliderHoursSize-xHoursCoord, 0).build().perform()
			WebUI.delay(1)
			actions.moveToElement(minutesInput).click().dragAndDropBy(minutesInput, sliderMinutesSize-xMinutesCoord, 0).build().perform()
		}

		defineWebElement("//div[@class=\'col s12\']",'xpath').click()
	}

	/**
	 * Pick your date automatically on calendar
	 *
	 * @param to Test Object
	 * @param date your date on calendar
	 * @param month your month on calendar
	 * @param year your year on calendar
	 * @author Azzam
	 */
	@Keyword
	public static void pickDate(TestObject to, int date, int month, int year) {
		def field = findWebElement(to)
		field.click()
		def currentMonth = new Date().format('M')
		def currentMonthConv = Integer.parseInt(currentMonth)-1
		def monthConv = month-1
		def dateInput

		if(year > 0 ){
			2.times{
				defineWebElement("//div[@class='datepicker -bottom-left- -from-bottom- active']//div[@class='datepicker--nav-title']", 'xpath').click()
			}

			def yearInput = defineWebElement("//div[@class='datepicker -bottom-left- -from-bottom- active']//div[@class='datepicker--cells datepicker--cells-years']//div[@data-year='$year']", 'xpath')

			actions.moveToElement(yearInput)

			actions.perform()

			yearInput.click()
		}

		if(month > 0){

			if(month > 0 && year == 0){
				defineWebElement("//div[@class='datepicker -bottom-left- -from-bottom- active']//div[@class='datepicker--nav-title']", 'xpath').click()
			}

			def monthInput = defineWebElement("//div[@class='datepicker -bottom-left- -from-bottom- active']//div[@class='datepicker--cells datepicker--cells-months']/div[@data-month='$monthConv']", "xpath")

			actions.moveToElement(monthInput)

			actions.perform()

			monthInput.click()

			dateInput = defineWebElement("//div[@class='datepicker -bottom-left- -from-bottom- active']//div[contains(@class,'datepicker--cell datepicker--cell-day')][@data-date='$date'][@data-month='$monthConv']", "xpath")
		} else {
			dateInput = defineWebElement("//div[@class='datepicker -bottom-left- -from-bottom- active']//div[contains(@class,'datepicker--cell datepicker--cell-day')][@data-date='$date'][@data-month='$currentMonthConv']", "xpath")
		}

		actions.moveToElement(dateInput)

		actions.perform()

		dateInput.click()

		defineWebElement("//div[@class=\'col s12\']",'xpath').click()
	}

	/**
	 * Pick value in drop-down
	 * 
	 * @param to
	 * @param String valueDropdown
	 * @author Azzam
	 */
	public static void pickDropdown(TestObject to, String valueDropdown){
		def field  = findWebElement(to)
		field.click()

		WebUI.delay(1)

		def element = Helper.defineWebElement("//span[contains(text(),'$valueDropdown')]", 'xpath')

		Common.actions.moveToElement(element)

		Common.actions.perform()

		element.click()
	}

	/**
	 * Looping pick value in drop-down
	 * 
	 * @param to
	 * @param List valueDropdown
	 * @author Azzam
	 */
	public static void pickDropdown(TestObject to, List valueDropdown){
		def field  = findWebElement(to)
		field.click()

		for(int i=0;i < valueDropdown.size();i++){
			def value = valueDropdown[i]
			WebUI.delay(1)

			def element = Helper.defineWebElement("//span[contains(text(),'$value')]", 'xpath')

			Common.actions.moveToElement(element)

			Common.actions.perform()

			element.click()

		}

	}

	/**
	 * Define web element
	 *
	 * @param locator Locator web element
	 * @param type Web element search method
	 * @return Return WebElement
	 */
	@Keyword
	public static WebElement defineWebElement(String locator, String type){
		WebElement defineElement

		type = type.toLowerCase()
		switch(type){
			case type = "xpath" :
				defineElement = driver.findElement(By.xpath(locator))
				break;
			case type = "id" :
				defineElement = driver.findElement(By.id(locator))
				break;
			case type = "cssselector" :
				defineElement = driver.findElement(By.cssSelector(locator))
				break;
			case type = "cssname" :
				defineElement = driver.findElement(By.className(locator))
				break;
			case type = "linktext" :
				defineElement = driver.findElement(By.linkText(locator))
				break;
			case type = "name" :
				defineElement = driver.findElement(By.name(locator))
				break;
			case type = "partiallink" :
				defineElement = driver.findElement(By.partialLinkText(locator))
				break;
			case type = "tag" :
				defineElement = driver.findElement(By.tagName(locator))
				break;
		}
		return defineElement
	}

	/**
	 * Define web element inner object
	 *
	 * @param object Base object to search
	 * @param locator Locator web element
	 * @param type Web element search method
	 * @return Return WebElement
	 */
	@Keyword
	public static WebElement defineWebElement(WebElement object, String locator, String type){
		WebElement defineElement

		type = type.toLowerCase()
		switch(type){
			case type = "xpath" :
				defineElement = object.findElement(By.xpath(locator))
				break;
			case type = "id" :
				defineElement = object.findElement(By.id(locator))
				break;
			case type = "cssselector" :
				defineElement = object.findElement(By.cssSelector(locator))
				break;
			case type = "cssname" :
				defineElement = object.findElement(By.className(locator))
				break;
			case type = "linktext" :
				defineElement = object.findElement(By.linkText(locator))
				break;
			case type = "name" :
				defineElement = object.findElement(By.name(locator))
				break;
			case type = "partiallink" :
				defineElement = object.findElement(By.partialLinkText(locator))
				break;
			case type = "tag" :
				defineElement = object.findElement(By.tagName(locator))
				break;
		}
		return defineElement
	}

	/**
	 * Define list of web element
	 *
	 * @param locator Locator web element
	 * @param type Web element search method
	 * @return Return List<WebElement>
	 */
	@Keyword
	public static List<WebElement> defineWebElements(String locator, String type){
		List<WebElement> defineElement

		type = type.toLowerCase()
		switch(type){
			case type = "xpath" :
				defineElement = driver.findElements(By.xpath(locator))
				break;
			case type = "id" :
				defineElement = driver.findElements(By.id(locator))
				break;
			case type = "cssselector" :
				defineElement = driver.findElements(By.cssSelector(locator))
				break;
			case type = "cssname" :
				defineElement = driver.findElements(By.className(locator))
				break;
			case type = "linktext" :
				defineElement = driver.findElements(By.linkText(locator))
				break;
			case type = "name" :
				defineElement = driver.findElements(By.name(locator))
				break;
			case type = "partiallink" :
				defineElement = driver.findElements(By.partialLinkText(locator))
				break;
			case type = "tag" :
				defineElement = driver.findElements(By.tagName(locator))
				break;
		}
		return defineElement
	}

	/**
	 * Define list of web element
	 *
	 * @param object Base object to search
	 * @param locator Locator web element
	 * @param type Web element search method
	 * @return Return List<WebElement>
	 */
	@Keyword
	public static List<WebElement> defineWebElements(WebElement object, String locator, String type){
		List<WebElement> defineElement

		type = type.toLowerCase()
		switch(type){
			case type = "xpath" :
				defineElement = object.findElements(By.xpath(locator))
				break;
			case type = "id" :
				defineElement = object.findElements(By.id(locator))
				break;
			case type = "cssselector" :
				defineElement = object.findElements(By.cssSelector(locator))
				break;
			case type = "cssname" :
				defineElement = object.findElements(By.className(locator))
				break;
			case type = "linktext" :
				defineElement = object.findElements(By.linkText(locator))
				break;
			case type = "name" :
				defineElement = object.findElements(By.name(locator))
				break;
			case type = "partiallink" :
				defineElement = object.findElements(By.partialLinkText(locator))
				break;
			case type = "tag" :
				defineElement = object.findElements(By.tagName(locator))
				break;
			default :
				println("Please insert the type correctly!")
		}
		return defineElement
	}

	@Keyword
	public static void selectByIndex(String xPath, int value){
		Select select = new Select(Helper.defineWebElement(xPath, "xpath"))
		select.selectByIndex(value)
		select = null
	}
}