import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.harukaedu.keywords.Helper as Helper
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

Helper.uploadFile(findTestObject('Studio/CoursesManagement/ImportTemplate/InpFile'), fileName)

WebUI.click(findTestObject('Studio/CoursesManagement/ImportTemplate/BtnImport'))

WebUI.click(findTestObject('Studio/CoursesManagement/ImportTemplate/BtnSave'))