import com.harukaedu.keywords.Helper as Helper
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

Helper.waitElementAndClick("//p[text()='$title']/parent::section/preceding-sibling::a")

WebUI.delay(1)

if(actions == 'Edit'){
	Helper.waitElementAndClick("//p[text()='$title']/parent::section/preceding-sibling::a/following-sibling::ul//a/i")
}

if(actions == 'Delete'){
	Helper.waitElementAndClick("//p[text()='$title']/parent::section/preceding-sibling::a/following-sibling::ul//button")
	
	WebUI.acceptAlert()
	
	Helper.waitElementVisible('//span[.=\'Post deleted\']//parent::div')
}