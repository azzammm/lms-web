import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.harukaedu.keywords.Helper as Helper
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

def materialName = 'SIM-LSPR-S02-Diskusi Minggu 2'

Helper.defineWebElement('//a[contains(text(),\'Kelas Aljabar (ALJBR) L\')]', 'xpath').click()

Helper.pickDropdown(findTestObject('Student/MyCourses/LearningActivity/SlctSection'), '14')

def learningMaterial = Helper.defineWebElement("//a[contains(text(),'$materialName')]/parent::div", 'xpath')

learningMaterial.click()

for (i = 11; i < 50; i++) {
    def message = "Testing automation nich bruh $i"
	
	def btnPost = Helper.defineWebElement("//a[contains(.,'Post your answer')]", 'xpath')
	
	WebUI.delay(1)

    btnPost.click()

	WebUI.delay(1)

    def messageToSend = Helper.defineWebElement('//textarea[@id=\'body\']', 'xpath')

    messageToSend.sendKeys("$message")

    def buttonSave = Helper.defineWebElement(messageToSend, '//parent::div/following-sibling::div[@class=\'actions\']/button', 
        'xpath')

    buttonSave.click()

    Helper.waitElementVisible("//a-scene[contains(.,'$message')]")
}