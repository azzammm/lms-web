import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.harukaedu.keywords.Helper as Helper
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

WebUI.click(findTestObject('Studio/DiscussionsManagement/Discussion/MenuDiscussionsManagement'))

WebUI.callTestCase(findTestCase('Studio/Essentials/DiscussionsManagement/EstlsUnitAction'), [('unitName') : 'Discussion from Katalon Edit'
        , ('actions') : 'Edit'], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Studio/DiscussionsManagement/Discussion/BtnShowPost'))

WebUI.callTestCase(findTestCase('Studio/Essentials/DiscussionsManagement/EstlsPostAction'), [('title') : 'Post from Katalon'
        , ('actions') : 'Edit'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Studio/Essentials/DiscussionsManagement/EstlsInputFormPost'), [('title') : 'Post from Katalon Edit'
        , ('body') : 'Post from Katalon Edit', ('maxReply') : '2000', ('partner') : 'Demo'], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Studio/General/BtnSave'))

Helper.waitElementVisible('//span[.=\'Post updated!\']//parent::div')