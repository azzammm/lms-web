import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

def dateOfBirth = '12/18/1996'

WebUI.click(findTestObject('General/BtnThreeDotMenu'))

WebUI.click(findTestObject('Student/Dashboard/StudentProfile/General/BtnEditProfile'))

WebUI.delay(1)

WebUI.click(findTestObject('Student/Dashboard/StudentProfile/Biodata/BtnEditMode'))

WebUI.delay(1)

WebUI.clearText(findTestObject('Student/Dashboard/StudentProfile/Biodata/InpDateOfBirth'))

WebUI.sendKeys(findTestObject('Student/Dashboard/StudentProfile/Biodata/InpDateOfBirth'), dateOfBirth)

WebUI.click(findTestObject('Student/Dashboard/StudentProfile/Biodata/BtnChangeBiodata'))

WebUI.delay(3)

assert WebUI.getAttribute(findTestObject('Student/Dashboard/StudentProfile/Biodata/InpDateOfBirth'), 'value') == dateOfBirth