import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.harukaedu.keywords.Helper as Helper
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

WebUI.click(findTestObject('Object Repository/Studio/NotificationManagement/General/MenuNotificationManagement'))

WebUI.click(findTestObject('Object Repository/Studio/General/BtnAddRed'))

WebUI.callTestCase(findTestCase('Studio/Essentials/NotificationManagement/EstlsInputForm'), [('partner') : 'Demo', ('recipients') : ['Lecturer','Student']
        , ('title') : 'Notification from Katalon', ('body') : 'Notificaiton from Katalon', ('url') : 'www.youtube.com', ('isPushNotification') : false], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Studio/General/BtnSave'))

Helper.waitElementVisible('//span[.=\'Notification created!\']//parent::div')

