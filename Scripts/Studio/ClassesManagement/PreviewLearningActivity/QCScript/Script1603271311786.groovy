import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.harukaedu.keywords.Helper as Helper
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

def className = 'Kelas Dasar Manajemen (MJ51710004) MJ20H'

WebUI.callTestCase(findTestCase('Studio/Essentials/ClassesManagement/General/EstlsClassesAction'), [('className') : className
        , ('actions') : 'Preview Learning Activity'], FailureHandling.STOP_ON_FAILURE)

int totalSection = Helper.defineWebElements('//input[@class=\'select-dropdown\']/following-sibling::ul//li', 'xpath').size()

for (int i = 0; i < totalSection; i++) {
    Helper.pickDropdown(findTestObject('Studio/ClassesManagement/PreviewLearningActivity/SlctSection'), (i + 1).toString())
	
	assert Helper.defineWebElement("//div/a[contains(text(),'Pre Quiz')]/span", 'xpath').getText() == "Unlimited"
}