import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

WebUI.click(findTestObject('Studio/ContentsManagement/MenuContentsManagement'))

WebUI.callTestCase(findTestCase('Studio/Essentials/ContentsManagement/General/EstlsContentAction'), [('listContentName') : [
            'Content from Katalon Assign Asset'], ('actions') : 'Edit'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Studio/Essentials/ContentsManagement/Content/EstlsContentInputForm'), [('title') : 'Content from Katalon Assign Asset Edit'
        , ('description') : 'Testing', ('tags') : 'qa, test, tech', ('contentType') : 'PMB', ('attempts') : 'Unlimited', ('partner') : 'Demo'], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Studio/ContentsManagement/Content/BtnSaveAssignAsset'))

WebUI.callTestCase(findTestCase('Studio/Essentials/ContentsManagement/Content/EstlsSearchAssets'), [('mapAssets') : [('AssetName') : [
                'ACT-S01-02-Sesi 9-12'], ('TypeAsset') : ['Video']]], FailureHandling.STOP_ON_FAILURE)