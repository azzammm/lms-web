import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

WebUI.callTestCase(findTestCase('Studio/Essentials/CoursesManagement/General/EstlsSearchCourses'), [('courseName') : 'Demo'], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Studio/Essentials/CoursesManagement/Unit/EstlsCoursesEditUnit'), [('sectionName') : 'SESI 3 - POLA KEGIATAN SUATU PEREKONOMIAN', ('unitName') : 'Post Quiz Azzam'
        , ('title') : 'Azzam change from Katalon', ('description') : '', ('startDate') : 0, ('dueDate') : 0, ('attempts') : '', ('tags') : '', ('topic') : ''
        , ('objective') : '', ('partner') : 'Demo'], FailureHandling.STOP_ON_FAILURE)