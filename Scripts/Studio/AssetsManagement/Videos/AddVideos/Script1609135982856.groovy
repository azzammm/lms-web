import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.harukaedu.keywords.Helper as Helper
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

WebUI.callTestCase(findTestCase('Studio/Essentials/AssetsManagement/EstlsDirectMenuAssets'), [('menu') : 'videos'], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Studio/General/BtnAddRed'))

WebUI.callTestCase(findTestCase('Studio/Essentials/AssetsManagement/Videos/EstlsInputFormVideo'), [('title') : 'Video from Katalon'
        , ('description') : 'Video from Katalon', ('isYoutube') : true, ('videoId') : 'TDUrIgkKFbU', ('transcript') : 'transcript_videos.txt'
        , ('tags') : 'tech, test, qa', ('partner') : 'Demo'], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Studio/General/BtnSave'))

Helper.waitElementVisible('//span[.=\'Video created!\']//parent::div')