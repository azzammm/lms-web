import com.harukaedu.keywords.Helper as Helper

def convertedWords = Helper.capitalizeFully(sectionName)

Helper.waitElementAndClick("//h2[contains(.,'$convertedWords')]/parent::div//following-sibling::ul//span[contains(.,'$unitName')]/following-sibling::a")

Helper.waitElementAndClick("//ul[@class='dropdown-content unit-template active']//a[contains(.,'delete_forever')]/input")

Helper.driver.switchTo().alert().accept();