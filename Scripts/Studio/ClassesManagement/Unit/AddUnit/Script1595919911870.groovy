import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

def listUnits = ['PKO-S03-Pre Quiz', 'SIM-LSPR-S02-03-Issue Management', 'PKO-S03-Presentasi', 'PKO-S03-02-Pemrosesan', 'SIM-LSPR-S02-Diskusi Minggu 2'
    , 'SIM-LSPR-S02-Practice Quiz', 'SIM-LSPR-S02-Post Quiz']

WebUI.callTestCase(findTestCase('Studio/Essentials/ClassesManagement/General/EstlsSearchClass'), [('className') : 'Kelas Aljabar (ALJBR) AAA'], 
    FailureHandling.STOP_ON_FAILURE)

listUnits.size().times({ 
        WebUI.callTestCase(findTestCase('Studio/Essentials/ClassesManagement/General/EstlsSectionAction'), [('sectionName') : 'Section Create From Katalon'
                , ('action') : 'Add Unit'], FailureHandling.STOP_ON_FAILURE)

        WebUI.callTestCase(findTestCase('Studio/Essentials/ClassesManagement/Unit/EstlsClassesAddUnit'), [('unitName') : listUnits[it]], 
            FailureHandling.STOP_ON_FAILURE)
    })